defmodule Lynx.Router do
  use Lynx.Web, :router
  # use Plug.ErrorHandler
  use Sentry.Plug

  pipeline :browser do
    plug :accepts, ["html", "json"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers

    plug :default_vars
    plug :set_user
  end

  pipeline :admin_browser do
    plug :accepts, ["html", "json"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers

    plug :default_vars
    plug :set_user
    plug :require_admin
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/admin", Lynx do
    pipe_through :admin_browser

    resources "/rss", AdminRssController
    get "/", AdminController, :index
  end

  scope "/", Lynx do
    pipe_through :browser

    post "/tag/search", TagController, :search
    post "/tag/autocomplete", TagController, :autocomplete

    # RESTful resources
    resources "/tag", TagController
    resources "/comments", LinkController, only: [:show]
    resources "/comments", CommentController, only: [:create, :update, :delete]
    resources "/session", SessionController, only: [:create, :delete]
    resources "/link", LinkController, only: [:create, :new, :edit, :update]
    resources "/link/read", LinkReadController, only: [:create, :delete]
    resources "/vote", VoteController, only: [:create]
    resources "/user", UserController, except: [:index, :delete]
    resources "/user/:user/subscriptions", UserSubscriptionController, only: [:index, :create, :delete]

    # Wildcard urls and non standard urls
    get "/comments/:id/thumbnail", LinkController, :thumbnail
    get "/tag/:id/submit", LinkController, :new
    get "/tag/:id/:sort", TagController, :show

    get "/sign_in", UserController, :new
    get "/submit", LinkController, :new
    get "/unread", LinkController, :index
    get "/hot", LinkController, :index
    get "/new", LinkController, :index
    get "/top", LinkController, :index
    get "/_ah/health", TagController, :index

    get "/", LinkController, :index
    get "/*path", SessionController, :not_found
  end
  # Other scopes may use custom stacks.
  # scope "/api", Lynx do
  #   pipe_through :api
  # end
end
