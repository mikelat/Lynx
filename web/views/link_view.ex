defmodule Lynx.LinkView do
  use Lynx.Web, :view
  require Integer

  # Returns url, or else comments
  def url_else_comments(conn, link) do
    if !is_nil(link.url), do: link.url, else: link_path(conn,:show,link.slug)
  end

  def sort_url(tag, sort \\ nil) do
    if tag.id do
      "/tag/#{tag.name_indexed}/#{sort}"
    else
      "/#{sort}"
    end
  end

  def tag_list(conn, link) do
    tags =
      Enum.map(link.tags, fn(tag) ->
        "<a href=\"#{tag_path(conn, :show, tag.name_indexed)}\" class=\"tag\">#{tag.name_indexed}</a>"
      end)

    # tags =
    #   case Link.content_type(link)[:type] do
    #     :image -> ["<span class=\"content\">pictures</span>" | tags]
    #     :video -> ["<span class=\"content\">videos</span>" | tags]
    #     :audio -> ["<span class=\"content\">sounds</span>" | tags]
    #     _ -> tags
    #   end

    tags =
      if link.nsfw do
        ["<span class=\"nsfw\">nsfw</span>" | tags]
      else
        tags
      end

    tags
    |> Enum.join(", ")
  end

  def comment_info(conn, comment) do
    time = time_ago comment.inserted_at
    user = case comment.deleted do
      true  -> "<em>deleted</em>"
      false -> "<a href=\"#{user_path(conn, :show, comment.user.name_lower)}\">#{comment.user.name}</a>"
    end
    raw "posted #{time} by #{user}"
  end

  def comment_options(conn, comment) do
    links =
      if Comment.can_delete?(comment, current_user(conn)) do
        ["<a href=\"#{comment_path(conn,:delete,comment.id)}\" class=\"delete\">delete</a>"]
      else
        []
      end
    links =
      if Comment.can_edit?(comment, current_user(conn)) do
        ["<a href=\"#{comment_path(conn,:update,comment.id)}\" class=\"update\" data-content=\"#{safe_to_string(html_escape(comment.text))}\">edit</a>" | links ]
      else
        links
      end
    links = ["<a href=\"#\" class=\"reply\">reply</a>" | links]
    raw "<li>#{Enum.join(links, "</li><li>")}</li>"
  end

  def even_odd_class(depth) do
    if Integer.is_even(depth), do: "even", else: "odd"
  end

  def comment_count(link) do
    to_string(link.comments_count)
    <> if(link.comments_count == 1, do: " comment", else: " comments")
  end

  def link_classes(conn, link) do
    class_list = ["link" <> mine_class(conn, link.user_id),
                  to_string(Enum.at(Link.types, link.content_type)[:type]),
                  votable_class(link)]
    class_list = if link.nsfw, do: ["nsfw" | class_list], else: class_list
    class_list = if link.thumbnail_generating, do: ["generating-thumbnail" | class_list], else: class_list

    class_list =
      if signed_in?(conn) && current_user(conn).unread_enable && Link.is_unread(link) do
        ["unread" | class_list]
      else
        class_list
      end

    Enum.join(class_list, " ")
  end

  def comment_classes(conn, comment) do
    class_list = ["comment" <> if(comment.deleted, do: " deleted", else: mine_class(conn, comment.user_id))]
    class_list = if !comment.deleted, do: [votable_class(comment) | class_list], else: class_list

    Enum.join(class_list, " ")
  end

  def tag_list_link(conn, change, vars) do
    math_change = if change == :next, do: 1, else: -1

    {url_var_name, url_var_value} =
      case vars[:sort] do
        # "after" pagination scheme
        x when x in [:new, :unread] ->
          {"start", vars[:link].slug_id}
        # typical pagination scheme
        _ ->
          {"page", to_integer(conn.query_params["page"]) + math_change}
      end
    params =
      conn.query_params
      |> Map.drop(["_pjax"])
      |> Map.put(url_var_name, url_var_value)
      |> Enum.map(fn({n,v}) -> "#{n}=#{v}" end)
      |> Enum.join("&")

    raw("#{change} <i class=\"fa fa-chevron-right\"></i>")
    |> link(class: "page push-right", to: "#{conn.request_path}?#{params}")
    |> safe_to_string
    |> raw
  end

  def tag_list_form(tags) do
    if Ecto.assoc_loaded?(tags) do
      Enum.map(tags, fn(x) -> x.name_indexed end)
      |> Enum.join(",")
    end
  end

  def thumbnail_url(link) do
    cond do
      link.thumbnail -> "#{config(:thumbnails_url)}#{link.thumbnail}.png"
      Link.content_type(link)[:type] == :text -> static_path(Lynx.Endpoint, "/images/text.png")
      :else -> static_path(Lynx.Endpoint, "/images/noimg.png")
    end
  end
end
