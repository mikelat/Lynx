defmodule Lynx.ErrorView do
  use Lynx.Web, :view

  def render("404.html", assigns) do
    error "page not found", assigns
  end

  def render("500.html", assigns) do
    error "internal server error", assigns
  end

  # In case no render clause matches or no
  # template is found, let's render it as 500
  def template_not_found(_template, assigns) do
    error "internal server error", assigns
  end

  defp error(message, assigns) do
    conn =
      assigns.conn
      |> Plug.Conn.assign(:title, "error")
      |> Plug.Conn.assign(:mini_title, "")

    try do # Try standard error first
      if json_request?(conn) do
        %{success: false, message: message}
      else
        render Lynx.ErrorView, "error.html", message: message, conn: conn,
          layout: {Lynx.LayoutView, "app.html"}
      end
    rescue # Worst case scenario - we couldn't even render the template
      _ ->
        raw("<h2>#{message}</h2>")
    end
  end
end
