defmodule Lynx.AppView do
  use Phoenix.HTML
  import Lynx.Session
  import Lynx.Utils

  def inline_js do
    inline_vars = %{
      recaptcha_key: config(:google_public_key),
      abuse_risk: abuse_risk()
    }
    raw "var env=#{Poison.encode!(inline_vars)};"
    <> "function gcap(){$(document).trigger(\"captcha:load\");}"
  end

  def captcha(name, risky \\ false) do
    class = "captcha" <> if risky, do: " risky", else: ""
    content_tag :div, "", class: class, id: name <> "_captcha"
  end

  def unread_counter(count) do
    count_display =
      cond do
        count > 99 -> "99+"
        count < 0  -> 0
        :else      -> count
      end
    class = if count == 0, do: "", else: " new"
    raw "<span class=\"unread-count#{class}\" data-count=\"#{count}\">#{count_display}</span>"
  end

  def vote_buttons do
    raw "<a class=\"up\" href=\"#\"><i class=\"fa fa-chevron-up\"></i></a>"
      <> "<a class=\"down\" href=\"#\"><i class=\"fa fa-chevron-down\"></i></a>"
  end

  def mine_class(conn, user_id) do
    if user_id == current_user(conn).id, do: " mine", else: ""
  end

  def signed_class(score) when score > 0, do: " positive"
  def signed_class(score) when score < 0, do: " negative"
  def signed_class(_), do: ""

  def votable_class(content) do
    if Ecto.assoc_loaded?(content.vote) && not is_nil(content.vote) do
      "votable"
        <> case content.vote.change do
          x when x > 0 -> " upped"
          x when x < 0 -> " downed"
          _            -> ""
        end
    else
      "votable"
    end
  end

  # def signed_score(score) when score >= 0, do: "+" <> to_string(score)
  # def signed_score(score), do: to_string(score)

  def active_class(boolean_result, class \\ "active") do
    if boolean_result, do: class
  end

  @minute 60
  @hour @minute * 60
  @day @hour * 24
  @month @day * 30
  @year @month * 12

  def time_ago(time) do
    Timex.from_now(time)

    # now = Timex.now
    # case Timex.diff(Timex.from(time), now, :seconds) do
    #   x when x <= 1 ->
    #     "just now"
    #   x when x < @minute ->
    #     num = Timex.Date.diff(now, time, :seconds)
    #     "#{num} #{pluralize(num, "second")} ago"
    #   x when x < @hour ->
    #     num = Timex.Date.diff(now, time, :minutes)
    #     "#{num} #{pluralize(num, "minute")} ago"
    #   x when x < @day * 2 ->
    #     num = Timex.Date.diff(now, time, :hours)
    #     "#{num} #{pluralize(num, "hour")} ago"
    #   x when x < @month ->
    #     num = Timex.Date.diff(now, time, :days)
    #     "#{num} #{pluralize(num, "day")} ago"
    #   x when x < @year ->
    #     num = Timex.Date.diff(now, time, :months)
    #     "#{num} #{pluralize(num, "month")} ago"
    #   _ ->
    #     num = Timex.Date.diff(now, time, :years)
    #     "#{num} #{pluralize(num, "year")} ago"
    # end
  end

  def time_ago_short(time) do
    now = Timex.now
    case Timex.diff(now, time, :seconds) do
      x when x < @minute ->
        "1m"
      x when x < @hour ->
        "#{Timex.diff(now, time, :minutes)}m"
      x when x < @day * 1.5 ->
        "#{Timex.diff(now, time, :hours)}h"
      x when x < @year ->
        "#{Timex.diff(now, time, :days)}d"
      _ ->
        "#{Timex.diff(now, time, :years)}y"
    end
  end

  def shorten_number(num) when num >= 1000 do
    num =
      Float.round(num / 1000, 1)
      |> Float.to_string()
      |> String.trim_trailing(".0")
    "#{num}k"
  end
  def shorten_number(num), do: "#{num}"
end
