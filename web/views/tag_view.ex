defmodule Lynx.TagView do
  use Lynx.Web, :view

  def subscribe_state(subscription \\ nil) do
    cond do
      subscription == nil  -> "state-unsubscribed"
      subscription.blocked -> "state-blocked"
      subscription.tag_id  -> "state-subscribed"
    end
  end
end
