defmodule Lynx.LayoutView do
  use Lynx.Web, :view
  @version String.slice(Mix.Project.config[:version], 5..-1)

  def data_vars(conn) do
    # title: title(conn), mini_title: safe_to_string(mini_title(conn))
    js =
      unless :admin_browser in conn.private[:phoenix_pipelines] do
        controller_class(conn)
      else
        conn.assigns[:js_exec]
      end
    data = [csrf: get_csrf_token(), body: body_classes(conn), js: js,
            unread_count: current_user(conn).unread_count]
    data = if conn.private[:abuse_score], do: [{:abuse_score, conn.private[:abuse_score]} | data], else: data
    #data = if conn.assigns[:category_path], do: Dict.put(data, :category_path, conn.assigns[:category_path]), else: data
    content_tag(:span, "", id: "vars", data: data)
  end

  def body_classes(conn) do
    global_class_list = ["c-#{controller_class(conn)}", "a-#{action_class(conn)}"]

    if signed_in?(conn) do
      class_list = ["signed-in" | global_class_list]
      class_list = if !current_user(conn).show_nsfw, do: ["hide-nsfw" | class_list], else: class_list
      class_list = if current_user(conn).unread_enable, do: ["unread-enable" | class_list], else: class_list
      class_list = if current_user(conn).unread_scroll, do: ["unread-scroll" | class_list], else: class_list
      class_list = if current_user(conn).unread_click, do: ["unread-click" | class_list], else: class_list
      class_list = if current_user(conn).unread_comment, do: ["unread-comment" | class_list], else: class_list
      class_list = if current_user(conn).weight_down > 0, do: ["can-downvote" | class_list], else: class_list
      Enum.join(class_list, " ")
    else
      Enum.join(["signed-out hide-nsfw" | global_class_list], " ")
    end
  end

  def alerts(conn) do
    Enum.map [:success, :info, :warning, :error], fn(status) ->
      message = get_flash(conn, status)
      if !is_nil(message) do
        render("_alert.html", message: message, status: status)
      else
        ""
      end
    end
  end

  def user_button(conn) do
    if signed_in?(conn) do
      link raw("<i class=\"icon fa fa-user-circle-o\"></i>#{current_user(conn).name}"), to: user_path(conn, :show, current_user(conn).name_lower), id: "user", class: "ink-button"
    else
      link raw("<i class=\"icon fa fa-sign-in\"></i>Sign in/Register"), to: "/sign_in", id: "user", class: "ink-button"
    end
  end

  def user_mobile_button(conn) do
    if signed_in?(conn) do
      link raw("<i class=\"icon fa fa-user\"></i>"), to: user_path(conn, :show, current_user(conn).name_lower), id: "user_mobile"
    else
      link raw("<i class=\"icon fa fa-sign-in\"></i>"), to: "/sign_in", id: "user_mobile"
    end
  end

  def title(conn) do
    if (is_nil(conn.assigns[:title]) || conn.request_path == "/") && conn.assigns[:kind] != :error do
      config(:site_name)
    else
      "#{conn.assigns[:title]} :: #{config(:site_name)}"
    end
    |> String.downcase
  end

  def mini_title(conn) do
    url = if conn.assigns[:mini_url], do: conn.assigns[:mini_url], else: conn.request_path
    raw(cond do
      conn.assigns[:mini_title] && url ->
        "<a href=\"#{url}\">#{conn.assigns[:mini_title]}</a>"
      conn.assigns[:title] ->
        "<a href=\"#{url}\">#{conn.assigns[:title]}</a>"
      :else ->
        ""
    end)
  end

  def submit_path(path \\ nil) do
    "/submit" <> if path, do: "/#{path}", else: ""
  end

  defp controller_class(conn) do
    if conn.assigns[:kind] == :error do
      "error"
    else
      conn
      |> Phoenix.Controller.controller_module
      |> to_string
      |> String.slice(12..-11)
      |> Macro.underscore
      |> String.downcase
    end
  end

  defp action_class(conn) do
    if conn.assigns[:kind] == :error do
      "error"
    else
      conn
      |> Phoenix.Controller.action_name
      |> to_string
      |> String.downcase
    end
  end

  def env_info do
    "#{config(:server_env)}#{@version}"
  end

  def google_tracking_code do
    raw "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){"
      <> "(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),"
      <> "m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)"
      <> "})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');"
      <> "ga('create', 'UA-49664142-5', 'auto');ga('send', 'pageview');"
  end
end
