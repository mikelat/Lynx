defmodule Lynx.SharedView do
  use Lynx.Web, :view

  def markdown_example(text) do
    raw "<dt>#{text}</dt><dd>#{MarkdownParser.convert(text)}</dd>"
  end

  def error_item(attr, message) do
    if attr == "base", do: message, else: "#{humanize(attr)} #{message}"
  end
end
