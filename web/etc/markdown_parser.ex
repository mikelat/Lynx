defmodule Lynx.MarkdownParser do
  use Phoenix.HTML

  @regex [
    simple: [
      {~r/(?<=\s|^)__(.+?)__(?=\s|$)/, "<strong>\\1</strong>"},
      {~r/(?<=\s|^)\*\*(.+?)\*\*(?=\s|$)/, "<strong>\\1</strong>"},
      {~r/(?<=\s|^|\>|\<)_(.+?)_(?=\s|$|\>|\<)/, "<em>\\1</em>"},
      {~r/(?<=\s|^|\>|\<)\*(.+?)\*(?=\s|$|\>|\<)/, "<em>\\1</em>"},
      {~r/(?<=\s|^|\>|\<)~~(.+?)~~(?=\s|$|\>|\<)/, "<del>\\1</del>"},
      {~r/(?<=\r\n|\n|^)+\s*&gt;\s*\!\s*(.+?)(?=\r\n|\n|$)/, "<div class=\"spoiler\">\\1</div>"},
      {~r/\^(.+?)\^/, "<sup>\\1</sup>"},
      {~r/~(.+?)~/, "<sub>\\1</sub>"}
    ],
    code: ~r/(?<=\s|^)\`(.+?)\`(?=\s|$)/ms,
    url: ~r/\[([^\]]+?)\]\(([^<>\'\"\s]+?)\)/,
    autolink: ~r/(?<=\s|^)((http:\/\/|https:\/\/|ftp:\/\/|www\.).+?)(?=\s|$)/,
    local_autolink: ~r/(?<=\s|^)(\/[a-zA-Z0-9\/\-]+)(?=\s|$)/,
    blockquote: ~r/(?<=\r\n|\n|^|\<blockquote\>)+\s*(&gt;.+(\r\n|\n|$))+/,
    list: ~r/(?:(?<=\r\n|\n|\A|^)(?:[^\S\n]*)(\d\.|\*|\-)\s+(.+(\r\n|\n|$)))+/
  ]

  @encode [
    {"&gt;", "&#x3E;"},
    {"`", "&#x60;"},
    {"_", "&#x5F;"},
    {"*", "&#x2A;"},
    {"~", "&#x7E;"},
    {"(", "&#x28;"},
    {")", "&#x20;"},
    {"[", "&#x5B;"},
    {"]", "&#x5D;"},
    {".", "&#x2E;"},
    {"-", "&#x2D;"}
  ]

  # Prevents infinite embedding of elements that potentially can ruin the browser
  @recursive_limit 3

  def convert(text) do
    text
    |> Plug.HTML.html_escape
    |> parse_code
    |> parse_simple(@regex[:simple])
    |> parse_lists
    |> parse_blockquotes
    |> parse_links
    |> String.replace(~r/(\r?\n){2,}/, "</p><p>")
    |> String.replace(~r/(\r?\n)/, "<br/>")
    |> tagify(:p)
  end

  defp parse_code(text) do
    Regex.replace(@regex[:code], text, fn _, match ->
      match
      |> parse_simple(@encode)
      |> tagify(:code)
    end)
  end

  defp parse_links(text) do
    text = Regex.replace(@regex[:url], text, fn _, title, url -> parse_link_tag(url, title) end)
    text = Regex.replace(@regex[:autolink], text, fn _, url -> parse_link_tag(url) end)
    Regex.replace(@regex[:local_autolink], text, fn _, url -> parse_link_tag(url) end)
  end

  defp parse_link_tag(url, title \\ "") do
    title = if String.length(title) == 0, do: url, else: title
    "<a href=\"#{url}\" target=\"_blank\">#{title}</a>"
  end

  defp parse_blockquotes(text, counter \\ 0)
  defp parse_blockquotes(text, counter) when counter >= @recursive_limit, do: text
  defp parse_blockquotes(text, counter) do
    Regex.replace(@regex[:blockquote], text, fn match ->
      match
      |> String.replace(~r/^\s*&gt;/m, "")
      |> tagify(:blockquote)
      |> parse_blockquotes(counter + 1)
    end)
  end

  defp parse_lists(text, counter \\ 0)
  defp parse_lists(text, counter) when counter >= @recursive_limit, do: text
  defp parse_lists(text, counter) do
    Regex.replace @regex[:list], text, fn match, seperator ->
      # The above regex for some reason doesn't match an optional space (it goes
      # to the next one) so I have to individually parse out the spacing here.
      # Could be bad regex or elixir, couldn't figure it out.
      spacing = List.last(Regex.run(~r/\A(\s*)(?:\d\.|\*|\-)/, match))
      match
      |> String.replace(~r/^#{Regex.escape(spacing)}(\d\.|\*|\-)\s/, "")
      |> String.split(~r/(?:\r\n|\n)#{Regex.escape(spacing)}(\d\.|\*|\-)\s*/)
      |> Enum.map(fn(x) -> parse_lists(x, counter + 1) end)
      |> Enum.join("</li><li>")
      |> tagify(:li)
      |> tagify(if seperator in ["*", "-"], do: :ul, else: :ol)
    end
  end

  defp parse_simple(text, []), do: text
  defp parse_simple(text, [{search, replace}|tl]) do
    text
    |> String.replace(search, replace)
    |> parse_simple(tl)
  end

  defp tagify(text, tag) do
    "<#{tag}>#{text}</#{tag}>"
  end
end