defmodule Lynx.Session do
  import Plug.Conn
  import Ecto.Query, only: [from: 2]
  import Lynx.Utils
  alias Lynx.Repo
  alias Lynx.User

  @long_max_age 10*365*24*60*60 # 10 years
  @abuse_risk 5 # Score required to start getting captchas
  @abuse_ban 10 # Score required to get temporarily banned
  @timeout 15 # Timeout in minutes

  def abuse_risk, do: @abuse_risk

  def sign_in_user(conn, user) do
    token = random_string(50)
    Repo.insert_all("users_auths", [[user_id: user.id, token: token]])
    conn
    |> put_resp_cookie("user", user.name_lower <> "&" <> token, max_age: @long_max_age, domain: System.get_env("URL"))
    |> put_session(:user_id, user.id)
    |> put_private(:current_user, user)
  end

  def sign_out_user(conn) do
    conn
    |> clear_session
    |> delete_resp_cookie("user", domain: System.get_env("URL"))
    |> put_private(:current_user, %User{})
  end

  def signed_in?(conn) do
    !is_nil(conn.private[:current_user].id)
  end

  def current_user(conn) do
    conn.private[:current_user]
  end

  def json_request?(conn) do
    header = Plug.Conn.get_req_header(conn, "content-type")
    header != [] && String.contains?(hd(header), "application/json")
  end

  def abuse_increment!(conn) do
    if !conn.private[:abuse_score] || conn.private[:abuse_score] == 0 do
      from(a in "users_abuses", where: a.ip_address == ^remote_ip(conn))
      |> Repo.delete_all
      Repo.insert_all("users_abuses", [[score: 1, ip_address: remote_ip(conn), inserted_at: Timex.now]])
      1
    else
      from(a in "users_abuses", where: a.ip_address == ^remote_ip(conn), update: [inc: [score: 1]])
      |> Repo.update_all([])
      conn.private[:abuse_score] + 1
    end
  end

  def get_abuse_query(conn) do
    time = Timex.shift(Timex.now, minutes: @timeout * -1)
    from(a in "users_abuses", select: [a.score, a.inserted_at],
     where: a.ip_address == ^remote_ip(conn) and a.inserted_at >= ^time)
    |> Repo.one || [0, nil]
  end

  def abuse_banned?(conn) do
    conn.private[:abuse_score] >= @abuse_ban
  end

  def abuse_check_captcha(conn, opts) do
    cond do
      conn.private[:abuse_score] == nil ->
        raise "Risk var not available"
      # is banned
      conn.private[:abuse_score] >= @abuse_ban ->
        {:ok, date} = Timex.Ecto.DateTime.load(conn.private[:abuse_inserted])
        minutes = @timeout - Timex.diff(Timex.now, Timex.to_datetime(date), :minutes)
        {false, "You have been temporarily locked out. Please try again in #{minutes} #{pluralize(minutes, "minute")}."}
      # risky under threshold
      opts[:risky] && !abuse_banned?(conn) ->
        {true, ""}
      # check captcha
      :else ->
        case Lynx.Recaptcha.verify(remote_ip(conn), opts[:response]) do
          {:error, message} -> {false, message}
          {:ok, _}          -> {true, ""}
        end
    end
  end
end