defmodule Lynx.Router.Plugs do
  alias Lynx.Repo
  alias Lynx.User
  import Plug.Conn
  import Lynx.{Session, Utils}
  import Ecto.Query, only: [from: 2]
  import Phoenix.Controller, only: [put_flash: 3, redirect: 2, json: 2]

  def set_user(conn, _opts \\ []) do
    user_id = get_session(conn, :user_id)
    conn =
      cond do
        # Not signed in
        !is_nil(user_id) ->
          user = Repo.get(User, user_id)
          {user, conn} = if is_nil(user), do: {%User{}, delete_session(conn, :user_id)}, else: {user, conn}
          put_private(conn, :current_user, user)
        # Potential user - credentials exist
        conn.cookies["user"] && String.contains?(conn.cookies["user"], "&") ->
          conn_a = set_abuse_score(conn)
          # User is brute force banned
          if abuse_banned?(conn_a) do
            put_private(conn, :current_user, %User{})
          else
            [username, password] = String.split(conn.cookies["user"], "&")
            user = Repo.one(from u in User,
                            join: ua in "users_auths", on: ua.user_id == u.id,
                            where: u.name_lower == ^username and ua.token == ^password,
                            select: u)
            # Valid user
            if !is_nil(user) do
              sign_in_user(conn, user)
            # Invalid user
            else
              abuse_increment!(conn_a)
              conn
              |> sign_out_user
              |> put_private(:current_user, %User{})
            end
          end
        :else ->
          put_private(conn, :current_user, %User{})
      end
    conn =
      if conn.private[:current_user].banned do
        conn
        |> put_flash(:error, "You are banned.")
        |> sign_out_user
        |> redirect(to: "/")
        |> halt
      else
        conn
      end

    Logger.metadata(user_id: conn.private[:current_user].id)
    Sentry.Context.add_breadcrumb(%{user_id: conn.private[:current_user].id})

    if current_user(conn).id && (current_user(conn).unread_date == nil
        || Timex.compare(Timex.now, current_user(conn).unread_date) >= 0) do
      put_private(conn, :current_user, User.rebuild_unread(current_user(conn)))
    else
      conn
    end
  end

  def default_vars(conn, _opts \\ []) do
    Sentry.Context.add_breadcrumb(%{ip_address: remote_ip(conn)})
    Logger.metadata(ip_address: remote_ip(conn))
    if get_session(conn, :phoenix_flash_json) do
      conn
      |> put_private(:phoenix_flash, get_session(conn, :phoenix_flash_json))
      |> delete_session(:phoenix_flash_json)
    else
      conn
    end
    |> assign(:category, nil)
    |> assign(:title, nil)
    |> put_resp_header("server", "4 8 15 16 23 42")
  end

  def set_abuse_score(conn, _opts \\ []) do
    [score, date] = get_abuse_query(conn)
    conn
    |> put_private(:abuse_score, score)
    |> put_private(:abuse_inserted, date)
  end

  # Redirects user to sign in page when not logged in with message
  def require_signed_in(conn, _opts \\ []) do
    if !current_user(conn).id do
      if json_request?(conn) do # JSON request
        conn
        |> json(%{success: false, message: "you are not signed in"})
        |> halt
      else # HTML request
        conn
        |> put_flash(:warning, "you need to sign in to continue")
        |> redirect(to: "/")
        |> halt
      end
    else
      conn
    end
  end

  def require_signed_out(conn, _opts \\ []) do
    if current_user(conn).id do
      if json_request?(conn) do # JSON request
        conn
        |> json(%{success: false, message: "you are already signed in"})
        |> halt
      else # HTML request
        conn
        |> put_flash(:warning, "you are already signed in")
        |> redirect(to: "/")
        |> halt
      end
    else
      conn
    end
  end

  # The require_admin plug purposely hides the error behind a 404, since we don't need people brute
  # forcing special admin urls
  def require_admin(conn, _opts \\ []) do
    if current_user(conn).admin, do: conn, else: raise Lynx.AdminRequired
  end
end
