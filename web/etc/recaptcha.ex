defmodule Lynx.Recaptcha do
  require Logger
  import Lynx.Utils

  @recaptcha_url "https://www.google.com/recaptcha/api/siteverify"

  def validate(model, remote_ip, response) do
    case verify(remote_ip, response) do
      {:error, message} -> Ecto.Changeset.add_error(model, :captcha, message)
      {:ok, _} -> model
    end
  end

  def verify(conn) when is_map(conn) do
    verify(conn.remote_ip, conn.params["g-recaptcha-response"])
  end

  def verify(remote_ip, response) when is_tuple(remote_ip) do
    verify(:inet_parse.ntoa(remote_ip), response)
  end

  def verify(remote_ip, response) do
    cond do
      # dev/test doesn't check captchas
      !config(:check_captcha) ->
        Logger.info "Bypassed Captcha check"
        {:ok, ""}
      response == "" ->
        {:error, "was not filled out"}
      :else ->
        case api_response(remote_ip, response) do
          %{"success" => true} ->
            {:ok, ""}
          %{"success" => false, "error-codes" => error_codes} ->
            {:error, api_error(error_codes)}
          %{"success" => false} ->
            {:error, "was invalid"}
        end
    end
  end

  defp api_response(remote_ip, response) do
    body_content = URI.encode_query(%{"remoteip" => to_string(remote_ip),
                                      "response" => response,
                                      "secret"   => config(:google_private_key)})
    headers = [{"Content-type", "application/x-www-form-urlencoded"}, {"Accept", "application/json"}]

    HTTPoison.post!(@recaptcha_url, body_content, headers, timeout: 5000).body
    |> Poison.decode!
  end

  defp api_error(errors) do
    case hd(errors) do
      "missing-input-response" -> "was not filled out"
      "invalid-input-response" -> "was incorrect"
      _ -> raise RuntimeError, "Recaptcha error, returned: #{Enum.join(errors, ", ")}"
    end
  end
end