defmodule Lynx.Utils do
  use Timex
  import Phoenix.Controller
  import Plug.Conn

  @random_pool Enum.to_list(?a..?z) ++ Enum.to_list(?0..?9)
  @random_upper_pool @random_pool ++ Enum.to_list(?A..?Z)

  def changeset_errors(changeset) do
    Ecto.Changeset.traverse_errors(changeset, fn {msg, opts} ->
      Enum.reduce(opts, msg, fn({key, value}, acc)  ->
        String.replace(acc, "%{#{key}}", to_string(value))
      end)
    end)
  end

  def random_string(length \\ 10, opts \\ []) do
    mixed_case = Keyword.get(opts, :mixed_case, true)
    Range.new(0, length)
    |> Enum.map(fn(_) -> Enum.random(if mixed_case, do: @random_upper_pool, else: @random_pool) end)
    |> to_string
  end

  def remote_ip(conn) do
    List.first(Plug.Conn.get_req_header(conn, "cf-connecting-ip"))
    || to_string(:inet_parse.ntoa(conn.remote_ip))
  end

  def redirect_json(conn, url) when is_binary(url) do
    redirect_json(conn, %{success: true, redirect: url})
  end

  def redirect_json(conn, url) when is_map(url) do
    conn =
      register_before_send conn, fn conn ->
        put_session(conn, "phoenix_flash_json", conn.private.phoenix_flash)
      end
    conn
    #|> Plug.Conn.put_status(302)
    |> json(url)
  end

  # Simple pluralization, I didn't need complicated regex plugins
  def pluralize(count, word, word_plural \\ nil) do
    if count == 1, do: word, else: word_plural || word <> "s"
  end

  def to_integer(number) do
    case Integer.parse(number || "") do
      {x, _} -> x
      _      -> 0
    end
  end

  def sort_by_key(models, key, result \\ %{})
  def sort_by_key([], _, result), do: result
  def sort_by_key([hd|tl], key, result) do
    id = if !is_nil(Map.get(hd, key)), do: Map.get(hd, key), else: 0
    result = if is_nil(result[id]), do: Map.put(result, id, []), else: result
    sort_by_key(tl, key, Map.put(result, id, result[id] ++ [hd]))
  end

  def config(key, default \\ nil) when is_atom(key) do
    case Application.get_env(:lynx, key) do
      {:system, env_var} ->
        case System.get_env(env_var) do
          nil     -> default
          "true"  -> true
          "false" -> false
          val     -> val
        end
      {:system, env_var, preconfigured_default} ->
        case System.get_env(env_var) do
          nil     -> preconfigured_default
          "true"  -> true
          "false" -> false
          val     -> val
        end
      nil -> default
      val -> val
    end
  end
end