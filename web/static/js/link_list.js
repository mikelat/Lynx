var utils = require("web/static/js/util")
var read = {
  queue_ids: [],
  queue_timeout: 0,
  // Save link id as read for user
  save: function(ids) {
    ids = $.isArray(ids) ? ids : [ids]
    $.ajax({ url: $("#link_list").data("read-url"), type: "POST",
             data: JSON.stringify({ "_method": "create",
                                    "_csrf_token": $("#vars").data("csrf"),
                                    "ids": ids })
    })
  },
  // Mark individual link in DOM as read
  mark: function(link) {
    if($(link).data("score") >= 0)
      read.counter(-1)
    $(link).removeClass("unread")
    $(".unread-label", link).html("<i class=\"fa fa-eye\" title=\"mark as read\"></i>")
  },
  // Queue up link id for being marked as read for user
  queue: function(id) {
    read.queue_ids.push(id)
    window.clearTimeout(read.queue_timeout)
    read.queue_timeout = window.setTimeout(read.queueSend, 600)
    read.mark($("#link_" + id))
  },
  // Send any link ids that exist in queue and clear it
  queueSend: function() {
    read.save(read.queue_ids)
    read.queue_ids = []
  },
  counter: function(change) {
    $(".unread-count").each(function(){
      var count = $(this).data("count") + change
      var count_display = count
      if(count > 99)
        count_display = "99+"
      else if(count < 0) {
        count = 0
        count_display = 0
      }
      $(this).removeClass("new").data("count", count).html(count_display)
      if(count != 0)
        $(this).addClass("new")
    })
  }
}

$(document).on("lynx:link-list", function(_, context) {
  $(".unread-label", context).click(function(){
    var id = utils.getID($(this).parents(".link").prop("id"))
    if($(this).parents(".link").hasClass("unread")) {
      read.save(id)
      read.mark($(this).parents(".link"))
      $(this).parents(".link").removeClass("unread-lock")
    } else {
      $.ajax({ url: $("#link_list").data("read-url") + "/" + id,
               type: "POST",
               data: JSON.stringify({"_method": "delete",
                                     "_csrf_token": $("#vars").data("csrf")})
      })
      if($(this).parents(".link").data("score") >= 0)
        read.counter(1)

      $(this).parents(".link").addClass("unread").addClass("unread-lock")
      $(this).html("<i class=\"fa fa-lock\" title=\"mark as read\"></i>")
    }
    return false
  })

  if($("body.unread-enable.unread-scroll").length) {
    $(window).on("scroll", function(){
      $.each($(".link.unread").not(".unread-lock"), function() {
        if($(window).scrollTop() > $(this).offset().top + $(this).outerHeight()) {
          read.queue(utils.getID($(this).prop("id")))
        } else {
          return false
        }
      })
    })
  }

  if($("body.unread-click").length) {
    $(".link.unread a.title, .link.unread .thumbnail a", context)
      .on("click auxclick contextmenu", function(){
      if(!$(this).parents(".link").hasClass("unread-lock")) {
        if(!$(this).parents(".link").hasClass("text")) {
          read.save(utils.getID($(this).parents(".link").prop("id")))
        }
        read.mark($(this).parents(".link"))
      }
    })
  }

  if($("body.unread-comment").length) {
    $(".link.unread .comments a", context).click(function(){
      // The read addition will happen on the comments page itself, only need to mark it as read
      read.mark($(this).parents(".link"))
    })
  }
})