var utils = require("web/static/js/util")
var tags_box = ""
var autocomplete = {
	queue_timeout: 0,
	spinner: function() {
		if(!$("#tags_autocomplete .loading-big").length) {
			$("#tags_autocomplete ul").html("<div class=\"loading-big\"><i class=\"fa fa-spinner fa-spin loading\"></i></div>")
			$("#tags_autocomplete").show()
		}
	},
	search: function() {
		$.post({ url: "/tag/autocomplete",
						 data: JSON.stringify({
							 "search": $(".taggle_input").val(),
							 "exclude": $("#link_tag_list").val(),
							 "_csrf_token": $("#vars").data("csrf")
						 })
		})
		.done(function(data){
			if(!data.tags.length) {
				// No tags found
				$("#tags_autocomplete ul").html("<li class=\"disabled\"><a href=\"#\" onclick=\"return false\">no matching tags found</a></li>")
			} else {
				// Add found tags to dropdown
				$("#tags_autocomplete ul").html("")
				$.each(data.tags, function(x) {
					var regexp = new RegExp(data.search.toLowerCase(), "gi")
					var replace = "<mark>" + data.search.toLowerCase() + "</mark>"
					var tag = data.tags[x].replace(regexp, replace)
					$("#tags_autocomplete ul").append(`<li><a href=\"#\" data-name=\"${data.tags[x]}\">${tag}</a></li>`)
				})
				$("#tags_autocomplete ul li:first").addClass("active")
				$("#tags_autocomplete ul li a").click(function(){
					tags_box.add($(this).data("name"))
					$("#tags_autocomplete").hide()
					$("#tags .taggle_input").focus()
					return false
				})
			}
		})
		.fail(function(){
			$("#tags_autocomplete ul").html(`<li class=\"disabled\"><a href=\"#\" onclick=\"return false\">server error</a></li>`)
		})
	}
}

$(document).on("lynx:ready", (_, context) => {
	// TODO: Remove the shitty tagging box
  if(!$("body.c-link, body.c-admin_rss").length) return

	require("web/static/js/link_list")
	$(document).trigger("lynx:link-list", context)

	$("#link_type_container input").change(function() {
		$("#link_text_container, #link_url_container").hide().removeClass("hide-all")

		if($("input[name='link[type]']:checked").val() == 1) {
			$("#link_text_container").show()
		} else {
			$("#link_url_container").show()
		}
	})

	if($("#form_tags_list", context).length) {
		var reset_tags = function(e, tag) {
			var tags = $("input[name^=taggles]")
									 .map(function() { return $(this).val() })
									 .get().join()
			$("#link_tag_list").val(tags)
		}

		var original_tags = $("#link_tag_list").val().split(",")
		if(original_tags[0] == "")
			original_tags = []

		tags_box = new Taggle("form_tags_list", {
			placeholder: false,
			tabIndex: false,
			onTagAdd: reset_tags,
			onTagRemove: reset_tags,
			tags: original_tags,
			submitKeys: [], //9, 13
			onBeforeTagAdd: function(e, tag) {
				return $(".taggle").length <= 5
			}
		});

		// enter, tab and comma
		$("#form_tags_list .taggle_input").keydown(function(e) {
			var key = window.event ? e.keyCode : e.which
			if(key == 9 || key == 13 || key == 188) {
				if($("#tags_autocomplete ul li.active").length) {
					tags_box.add($("#tags_autocomplete ul li.active a").data("name"));
					$("#tags_autocomplete").hide()
				}
				e.preventDefault()
				return false
			}
		})

		$("#form_tags_list .taggle_input").keyup(function(e) {
			var key = window.event ? e.keyCode : e.which
			// up and down
			if((key == 40 || key == 38) && $("#tags_autocomplete ul li.active").length) {
				var current = $("#tags_autocomplete ul li.active")
				current.removeClass("active")
				if(key == 40) {
					current.next().addClass("active")
					if(!$("#tags_autocomplete ul li.active").length) {
						$("#tags_autocomplete ul li:first").addClass("active")
					}
				} else {
					current.prev().addClass("active")
					if(!$("#tags_autocomplete ul li.active").length) {
						$("#tags_autocomplete ul li:last").addClass("active")
					}
				}
				return false
			} else if(key == 27) {
				// escape
				$("#tags_autocomplete").hide()
			} else if(key == 9 || key == 13 || key == 188) {
				// enter, tab and comma, action already happened on keydown
				e.preventDefault()
				return false
			} else {
				clearTimeout(autocomplete.queue_timeout)
				if($(this).val() == "") {
					$("#tags_autocomplete").hide()
				} else {
					autocomplete.spinner()
					autocomplete.queue_timeout = window.setTimeout(autocomplete.search, 300)
				}
			}
		})
		$(document).mouseup(function (e) {
			var container = $("#tags_autocomplete");
			if (!container.is(e.target) && container.has(e.target).length === 0) {
				container.hide()
			}
		});
	}

	// Submit when changing days from "top"
	$("#days", context).change(function(){
		$(this).parents("form").submit()
	})

	$("#link_params", context).submit(function(){
		$("#link_params input, #link_params select").each(function(index, obj){
      if($(obj).val() == "") {
        $(obj).prop("name", "");
      }
    });
    if($("#days").val() == "forever") {
    	$("#days").prop("name", "");
    }
	})

	// Comments page
	$(".reply, .update", ".comment", context).click(function() {
		if($("body.signed-out").length) {
      window.location = $("#user").attr("href");
      return false;
    }
		var type = $(this).hasClass("update") ? "update" : "reply"
		var id = $(this).parents('.comment').prop("id").match(/comment_([0-9]+)/)[1]
		var form = $(`#${type}_form_${id}`)
		$(this).parents(".comment").find("form").not(form).hide()
		if(form.length) {
			form.toggle()
		} else {
			form = $("#comment_form").clone(true, true)
			form.prop("id", type + "_form_" + id).addClass("form-" + type)
			$("[id]", form).each(function(){
				$(this).prop("id", $(this).prop("id") + "_" + id)
			})
			$("input[name='comment[comment_id]']", form).val(id)
			$(".errors", form).remove()
			if(type == "update") {
				form.attr("action", $(this).attr("href"))
				form.prepend("<input type=\"hidden\" name=\"_method\" value=\"put\">")
			}
			$("#comment_" + id + " > .comment-options").after(form)
			form = $(`#${type}_form_${id}`)
		}

		if(form.is(":visible")) {
			$("textarea", form).focus()
			if(type == "update") {
				$("textarea", form).val($(this).data("content"))
			}
		}
		return false
	})
	// Procs when comment is successfully submitted and its a edit
	// Replaces body with new text content
	$("#comment_form", context).on("form:success", function(obj, data) {
		if($(this).hasClass("form-update")){
			var comment = $(this).parents(".comment")
			$(".comment-markdown", comment).html(data.text_cached)
			$(".update", comment).data("content", data.text)
			$(".form-update", comment).hide()
		}
	})
	// Delete Comment
	$("section.comment .delete", context).click(function() {
		if(!confirm("Are you sure you wish to delete this comment?")) { return false }
		var token = $("#comment_form input[name=_csrf_token]").val()
		var comment = $(this).parents("section.comment")
		$.ajax({ url: $(this).attr("href"),
						 type: "POST",
						 data: JSON.stringify({"_method": "delete", "_csrf_token": token})
		})
		.success(function(data) {
			var new_comment = $.parseHTML(data.comment)
			comment.attr("class", $(".comment", new_comment).attr("class"))
			$(".comment-header:first", comment).html($(".comment-header", new_comment).html())
			$(".markdown:first", comment).html($(".markdown", new_comment).html())
		})
		.fail(function() { alert("There was an error deleting this comment.") })
		return false
	})
})