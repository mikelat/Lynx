var utils = require("web/static/js/util")
$.ajaxSetup({contentType:"application/json; charset=utf-8"})
require("web/static/js/link_list")
//$.pjax.defaults.timeout = 5000
//NProgress.configure({showSpinner:false})
var was_loaded = false
Turbolinks.start()
$(document)
.on("turbolinks:load", () => {
  if($("#vars").data("js").length)
    require("web/static/js/" + $("#vars").data("js"))
  $(window).off()
  $(document).trigger("lynx:ready") //, "#content"
  $("body").attr("class", $("#vars").data("body"))
  var path = $("#vars").data("category-path")
  path = (typeof path == "undefined" ? "" : path)
  $("aside .submit a").attr("href", path + "/submit")
  var unread_count = $("#vars").data("unread-count")
  $("aside .unread-count")
    .removeClass("new")
    .data("count", unread_count)
    .html(unread_count > 99 ? "99+" : unread_count)
  if(unread_count != 0)
    $("aside .unread-count").addClass("new")
  if(was_loaded && typeof ga != "undefined") {
    ga("set", "page", window.location.pathname)
    ga("send", "pageview")
  }
  else
    was_loaded = true
})
.on("lynx:ready", (_, context) => {
  $("#sign_out", context).click(function(){
    $(document).trigger("lynx:goto", [$(this).attr("href"), "delete"])
    return false
  })
  var elements = document.querySelectorAll('[data-submit^=parent]')
  var len = elements.length
  // Links turning into form submission with irregular routes
  $("[data-submit^=parent]", context).click(function(){
    if(!$(this).data("confirm") || confirm($(this).data("confirm")))
      $(this).parents("form").submit()
    return false
  })
  // Confirm message on links
  $("a[data-confirm]", context).not("[data-submit^=parent]").click(function() {
    return confirm($(this).data("confirm"))
  })
  // This is here until other modern browsers fix the noopener vunerability
  $("a[rel=noopener]", context).click(function(){
    var otherWindow = window.open()
    otherWindow.opener = null
    otherWindow.location = $(this).attr("href")
    return false
  })
  // Markdown field toggle
  $(".markdown-field a.toggle").click(function(){
    $(".markdown-help", $(this).parents(".markdown-field")).toggle()
    return false
  })
  $(document).trigger("captcha:load")
  // Voting
  $(".up, .down", context).click(function(){
    // Not logged in, redirect to login page
    if($("body.signed-out").length) {
      window.location = $("#user").attr("href");
      return false;
    }
    var change = 0,
        votable = $(this).closest(".votable"),
        weight_up = 1, // at some point there was weighed voting but whatever
        weight_down = 1,
        new_score = parseInt(votable.data("score")),
        new_class = "",
        is_removal = (($(this).hasClass("up") && votable.hasClass("upped"))
                     || ($(this).hasClass("down") && votable.hasClass("downed"))),
        is_new = !(votable.hasClass("downed") || votable.hasClass("upped"));

    votable.removeClass("upped downed");

    if($(this).hasClass("up")) {
      change = 1;
      if(!is_removal)
        votable.addClass("upped")
    }
    else {
      change = -1;
      if(!is_removal)
        votable.addClass("downed")
    }

    // Score reversed since its a removal
    if(is_removal) {
      change *= -1;
    }

    // Ajax post score parameters
    $.ajax({ url: "/vote", type: "POST",
             data: JSON.stringify({ "_csrf_token" : $("#vars").data("csrf"),
                                    "content": utils.getContentType(votable.prop("id")),
                                    "id": utils.getID(votable.prop("id")),
                                    "change": is_removal ? 0 : change })
    })
    .success(function(data){
      if(data.sign_in) { window.location = $("#user").attr("href"); }
    })
    .fail(function() { alert("There was an error saving your vote.") });

    // Calculate new score locally
    if(change == 1) {
      if(!is_removal) { new_score += weight_up }
      if(!is_new) { new_score += weight_down }
    } else {
      if(!is_removal) { new_score -= weight_down }
      if(!is_new) { new_score -= weight_up }
    }

    // Determine positive/negative colouring class
/*    if(new_score > 0)
      new_class = "positive"
    else if(new_score < 0)
      new_class = "negative"*/

/*    votable.data("score", new_score)
    if(new_score >= 0) {
      new_score = "+" + new_score
    }*/
    $("span", this).html(new_score)
    votable.data("score", new_score)
    // Apply score locally
    // $("*[data-score-display]", votable).each(function(){
    //   //$(this).removeClass("positive negative").addClass(new_class)

    // });
    return false
  });
  // Open menu
  $("#menu", context).click(function(){
    if($("body.side-open").length) {
      $("aside").scrollTop(0)
      $("body").removeClass("side-open")
      $("body").addClass("side-close")
    } else {
      $("body").addClass("side-open");
      $("body").removeClass("side-close");
    }
    return false
  })
  // Open user menu
  // $("#user_menu", context).click(function(){
  //   if($("body.signed-out").length) {
  //     return true
  //   }
  //   if($("body.user-open").length) {
  //     $("body").removeClass("user-open");
  //   } else {
  //     $("body").addClass("user-open side-close");
  //     $("body").removeClass("side-open");
  //     $("#user_nav").scrollTop(0);
  //   }
  //   return false
  // });
  // New thumbnail keeps checking for processing
  var loaded_time = Date.now()
  $("section.link.generating-thumbnail").each(function(){
    try_thumbnail($(this), loaded_time);
  })
  // Alert dismissal button
  $(".ink-dismiss", context).click(function(){
    $(this).parents(".ink-alert").hide()
    return false
  })
  $(".toggle-password", context).click(function(){
    var target = $("input[type=text], input[type=password]", $(this).parents(".control"))
    if(target.prop("type") == "text")
      target.prop("type", "password")
    else
      target.prop("type", "text")
    $(this).blur()
    return false
  })
  // Category subscribing/unsubscribing button functionality
  $(".subscribe button", context).click(function() {
    var id = utils.getID($(this).parents(".subscribe").prop("id"))
    var url = ""
    // Unsubscribing
    if($("#subscriptions_list", context).length) {
      url = $("#subscriptions_list", context).data("url")
    } else {
      url = $("#my_subscriptions").attr("href")
    }
    if($(this).hasClass("button-subscribed") || $(this).hasClass("button-blocked")) {
      $.ajax({ url: url + `/${id}`, type: "POST",
               data: JSON.stringify({"_method": "delete",
                                     "_csrf_token" : $("#vars").data("csrf")})
      })
      .success(sidebar_reload);
      $(this).parents(".subscribe")
        .removeClass("state-subscribed state-blocked")
        .addClass("state-unsubscribed")
    } else {
      // Subscribing
      $.ajax({url: url, type: "POST",
              data: JSON.stringify({"_method": "create",
                                    "_csrf_token" : $("#vars").data("csrf"),
                                    "id": id,
                                    "blocked": $(this).hasClass("button-block")})
      })
      .success(sidebar_reload);

      $(this).parents(".subscribe").removeClass("state-unsubscribed")
      if($(this).hasClass("button-block"))
        $(this).parents(".subscribe").addClass("state-blocked")
      else
        $(this).parents(".subscribe").addClass("state-subscribed")

    }
    $(this).blur();
    return false;
  });
  $("form[method=post]:not(.no-ajax):not(.link)", context)
  // Prevent the form from submitting when pressing "enter" when the autocomplete window is open
  .keydown(function(e) {
    var key = window.event ? e.keyCode : e.which;
    if((key == 13 || key == 9) && $(".autocomplete-suggestions:visible").length)
      return false
  })
  .submit(function() {
    var form = $(this);
    $(".errors", this).remove()
    $(".status", this).hide()
    $(".status.saving", this).show()
    $("button[type=submit]", form).prop("disabled", true);
    $.post($(this).attr("action"), utils.serializeFormJson($(this)))
    .always(function(xhr, statusText, xhr2) {
      xhr = xhr.readyState ? xhr : xhr2;
      $(form).trigger("form:always", xhr);
      $(".status", form).hide()
      // Success
      if(xhr.status >= 200 && xhr.status <= 308) {
        var data = jQuery.parseJSON(xhr.responseText)
        $(form).trigger("form:success", data, xhr)
        if(data.success == true) {
          $(".status.saved", form).show()
          if(typeof data.redirect != "undefined" && data.redirect.length > 0) {
            if(form.hasClass("no-turbolinks"))
              window.location = data.redirect
            else
              Turbolinks.visit(data.redirect)
            return false
          }
          else if(typeof data.refresh != "undefined") {
            location.reload()
            return false
          }
        } else {
          if(typeof data.message != "undefined") {
            $(".status.error", form).show()
            alert(data.message)
          } else {
            $(form).trigger("form:error", xhr)
            $(".status.error", form).show()
            $(".captcha", form).each(function() {
              if($(this).html())
                grecaptcha.reset($(this).data("captcha"))
            });
            if(typeof data.abuse_score != "undefined") {
              $("#vars").data("abuse_score", data.abuse_score)
              $(document).trigger("captcha:load")
            }

            var errors = "<ul class=\"errors\">";
            $.each(data.errors, function(key, value){
              if(key == "base") {
                $.each(value, function(k, v) {
                  errors += `<li>${v}</li>`
                })
              } else {
                $.each(value, function(k, v){
                  errors += `<li>${utils.humanizeString(key)} ${v}</li>`
                })
              }
            });
            $(":visible:not(:header):first", form).before(errors + "</ul>")
            if(form.prop("id")) {
              if($('#' + form.prop("id")).offset().top < 60)
                $(document).scrollTop(0)
              else
                $(document).scrollTop($('#' + form.prop("id")).offset().top)
            }
          }
        }
      } else {
      // Error
        $(".status.error", form).show()
        alert("an error has occured, try again later");
        console.log(xhr)
      }
      $("button[type=submit]", form).prop("disabled", false)
    })
    return false
  });
})
.on("captcha:load", (_, context=null) => {
  if($(".captcha", context).length && typeof grecaptcha !== "undefined") {
    var extra = parseInt($("#vars").data("abuse_score")) >= env.risk_threshold ? "" : ":not(.risky)";
    $(".captcha" + extra, context).each(function() {
      if(!$(this).html()) {
        $(this).data("captcha", grecaptcha.render(
          $(this).prop("id"), { sitekey: env.recaptcha_key }
        ));
      }
    });
  }
})
.on("lynx:goto", (_, url, method) => {
  $.ajax({url: url,
          type: "POST",
          data: JSON.stringify({"_method": method, "_csrf_token": $("#vars").data("csrf")})
  })
  .always(function(xhr, statusText, xhr2) {
    var xhr = xhr.readyState ? xhr : xhr2;
    // Success
    if(xhr.status >= 200 && xhr.status <= 308) {
      var data = jQuery.parseJSON(xhr.responseText);
      if(data.success)
        window.location = data.redirect;
    }
  })
})

function try_thumbnail(link, loaded_time) {
  $.get($("a.comments", link).attr("href") + "/thumbnail")
  .done(function(data){
    if(data.thumbnail) {
      link.removeClass("generating-thumbnail")
      $(".thumbnail img", link).attr("src", data.thumbnail)
    } else {
      // Only attempts to find thumbnail for 20 seconds
      if(Date.now() - 20000 > loaded_time) {
        link.removeClass("generating-thumbnail")
      } else {
        setTimeout(function(){try_thumbnail(link, loaded_time)}, 2000)
      }
    }
  })
}

function sidebar_reload(data) {
  if(data.success == true) {
    $("aside *").off();
    var new_sidebar = $.parseHTML(data.sidebar);
    $("aside").html(new_sidebar[0].innerHTML);
    $(document).trigger("lynx:ready", "aside");
  } else {
    alert("Error! Subscription not saved!")
  }
}
