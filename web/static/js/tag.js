var utils = require("web/static/js/util")

$(document).on("lynx:ready", (_, context) => {
  if(!$("body.c-tag").length) return

  $(document).trigger("lynx:link-list", context)
  if($("body.c-tag.a-index", context).length) {
    var search_timeout = 0
    $("#tag_search", context).keyup(function(){
      search_spinner()
      clearTimeout(search_timeout)
      search_timeout = setTimeout(search, 300)
    })
  }
  $("#tag_name", context).keyup(function() {
    var tag_preview = $.trim($("#tag_name").val())
                       .toLowerCase()
                       .replace(/[^a-z0-9]+/g,'')
                       .substring(0, 10)
    $("#tag_url_preview span").html(tag_preview)
  })
})

function search_spinner() {
  if(!$(".tag_list .loading-big").length) {
    $(".tag_list").html("<div class=\"loading-big\"><i class=\"fa fa-spinner fa-spin loading\"></i></div>")
  }
}

function search() {
  search_spinner()
  $.ajax({url: $("#tag_form").attr("action") + "/search",
          type: "POST",
          data: JSON.stringify({"_csrf_token": $("#vars").data("csrf"),
                                "search": $("#tag_search").val()})
  })
  .success(function(data){
    if(data["success"]) {
      $(".tag_list").html(data["html"])
      $(document).trigger("lynx:ready", ".tag_list")
      if(data.search) {
        $(".tag_list li a").each(function(){
          var regexp = new RegExp(data.search.toLowerCase(), "gi")
          var replace = "<mark>" + data.search.toLowerCase() + "</mark>"
          $(this).html($(this).html().replace(regexp, replace))
        })
      }
    }
  })
}