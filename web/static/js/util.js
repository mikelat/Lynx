module.exports = {
  serializeFormJson: (form) => {
    var data = {"_format": "json"}
    $.each($(form).serializeArray(), function(i, v) {
      var matches = v.name.match(/([a-z0-9_]+)\[([a-z0-9_]+)\]/)
      if(matches == null) {
        data[v.name] = v.value
      } else {
        if(typeof data[matches[1]] == "undefined") {
          data[matches[1]] = {}
        }
        data[matches[1]][matches[2]] = v.value
      }
    })
    return JSON.stringify(data)
  },
  humanizeString: (str) => {
    return str
      .replace(/_/g, ' ')
      .trim()
      .replace(/^[a-z]/g, function(first) {
        return first.toUpperCase()
      })
  },
  getID: (str) => {
    return str.match(/[a-z]+_(.+)/)[1]
  },
  getContentType: (str) => {
    return str.match(/([a-z]+)_.+/)[1]
  }
}