$(document).on("lynx:ready", (_, context) => {
  if(!$("body.c-user").length) return
  $("#settings_form input", context).change(function(){
    $("#settings_form").submit()
    if($(this).is(":checked")){
      $("body").removeClass($(this).data("body-off"))
      $("body").addClass($(this).data("body-on"))
      $($(this).data("field-enable")).prop("disabled", false)
      $($(this).data("field-disable")).prop("disabled", true)
    } else {
      $("body").removeClass($(this).data("body-on"))
      $("body").addClass($(this).data("body-off"))
      $($(this).data("field-enable")).prop("disabled", true)
      $($(this).data("field-disable")).prop("disabled", false)
    }
  })
})