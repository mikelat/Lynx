defmodule Lynx.Rss do
  use Lynx.Web, :model

  schema "rss" do
    field :name, :string
    field :url, :string
    field :title, :string, default: "{title}"
    field :search, :string
    field :replace, :string
    field :match, :string
    field :tag_list, :string
    field :next_parse, Timex.Ecto.DateTime
    field :last_parse, Timex.Ecto.DateTime
    field :hashes, :string
    field :minutes_lower, :integer, default: 30
    field :minutes_upper, :integer, default: 45
    field :active, :boolean, default: false
    field :clean_url, :boolean, default: false

    timestamps()
  end

  @required_fields ~w(name url title tag_list minutes_lower minutes_upper active)a
  @optional_fields ~w(search replace match clean_url)a

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ %{}) do
    model
    |> cast(params, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> validate_change(:search, &validate_regex/2)
    |> validate_change(:match, &validate_regex/2)
    |> validate_format(:tag_list, ~r/[A-Za-z0-9,]/i)
    |> validate_feed
  end

  def validate_feed(changeset) do
    if changeset.valid? && get_change(changeset, :url) do
      {status, feed} = Rss.fetch_feed(get_change(changeset, :url))
      cond do
        status == :error                   -> add_error(changeset, :base, "Could not load feed!")
        is_nil(changeset.data.inserted_at) -> change(changeset, hashes: mark_all_read(feed))
        :else                              -> changeset
      end
    else
      changeset
    end
  end

  # Sets feed as completely read (all hashes in array)
  def mark_all_read(feed) do
    feed
    |> Enum.map(&hash/1)
    |> Enum.join(",")
  end

  defp validate_regex(atom, value) do
    {result, error} = Regex.compile(value)
    if result == :error do
      [{atom, {"regex compilation error: #{elem(error,0)} near character #{elem(error,1)}", []}}]
    else
      []
    end
  end

  def fetch_feed(url) when is_binary(url) do
    try do
      with {:ok, %HTTPoison.Response{body: body}}     <- HTTPoison.get(url, [], [hackney: [follow_redirect: true]]),
           {:ok, %FeederEx.Feed{entries: entries}, _} <- FeederEx.parse(body)
      do
        {:ok, entries}
      else
        _ -> {:error, "Could not fetch feed"}
      end
    rescue
      ErlangError -> {:error, "Could not parse feed"}
    end
  end
  def fetch_feed(model), do: fetch_feed(model.url)

  def parse_title(model, item) do
    keys = Map.keys(item) |> List.delete(:__struct__)
    title = parse_item_title(model.title, keys, item)
    title =
      if (model.search || "") != "" do
        {:ok, search} = Regex.compile(model.search)
        String.replace(title, search, model.replace || "")
      else
        title
      end
    title =
      if (model.match || "") != "" do
        {:ok, match} = Regex.compile(model.match)
        if String.match?(title, match), do: title, else: ""
      else
        title
      end
    title
  end

  def hash(item) do
    :crypto.hash(:sha, item.id || item.title) |> Base.encode16
  end

  defp parse_item_title(title, keys, _) when keys == [], do: title
  defp parse_item_title(title, keys, item) do
    current_item = hd(keys)
    if is_binary(Map.get(item, current_item)) do
      title
      |> String.replace("{#{current_item}}", Map.get(item, current_item))
      |> parse_item_title(tl(keys), item)
    else
      title
    end
    |> parse_item_title(tl(keys), item)
  end
end
