defmodule Lynx.UserSubscription do
  use Lynx.Web, :model

  schema "users_subscriptions" do
    field :blocked, :boolean, default: false

    belongs_to :tag, Tag
    belongs_to :user, User

    timestamps()
  end

  @required_fields ~w(tag_id user_id)a
  @optional_fields ~w(blocked)a

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ %{}) do
    model
    |> cast(params, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
  end

  def insert(changeset) do
    case Repo.insert(changeset) do
      {:ok, model} ->
        Tag.inc_subscription_count(model.tag_id)
        {:ok, model}
      {:error, changeset} -> {:error, changeset}
    end
  end

  def insert!(changeset) do
    case insert(changeset) do
      {:ok, model} -> model
      {:error, changeset} ->
        raise Ecto.InvalidChangesetError, action: :insert, changeset: changeset
    end
  end

  def delete(model) do
    case Repo.delete(model) do
      {:ok, model} ->
        Tag.inc_subscription_count(model.tag_id, -1)
        {:ok, model}
      {:error, changeset} -> {:error, changeset}
    end
  end

  def delete!(model) do
    case delete(model) do
      {:ok, model} -> model
      {:error, changeset} ->
        raise Ecto.InvalidChangesetError, action: :delete, changeset: changeset
    end
  end

  def get_user_subscriptions(user_id) do
    from(s in UserSubscription, where: s.user_id == ^user_id, select: {s.tag_id, s.blocked})
    |> Repo.all
    |> Enum.group_by(fn({_, blocked}) -> if blocked, do: :blocked, else: :subscribed end, fn({tag_id, _}) -> tag_id end)
  end
end
