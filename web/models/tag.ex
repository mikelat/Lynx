defmodule Lynx.Tag do
  use Lynx.Web, :model

  schema "tags" do
    field :name, :string
    field :name_indexed, :string
    field :subscribers, :integer, default: 0
    field :default, :boolean, default: false
    field :can_submit, :boolean, default: true
    field :ip_address, :string

    #belongs_to :tag, Tag
    many_to_many :links, Link, join_through: "links_tags", on_delete: :delete_all
    has_one :subscription, UserSubscription
    has_many :subscriptions, UserSubscription, on_delete: :delete_all
    belongs_to :user, User

    timestamps()
  end

  @sorting_names ~w(submit top new unread)a

  @required_fields ~w(name)a
  @optional_fields ~w(name_indexed)a

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ %{}, opts \\ []) do
    changeset = cast(model, params, @required_fields ++ @optional_fields)

    if is_nil(changeset.data.inserted_at) && !opts[:force] && opts[:ip_address] do
      Lynx.Recaptcha.validate(changeset, opts[:ip_address], opts[:captcha])
    else
      changeset
    end
    |> validate_required(@required_fields)
    |> set_name_indexed
    |> update_change(:name, &String.strip/1)
    |> validate_length(:name, min: 2, max: 30)
    |> validate_format(:name, ~r/^[A-Za-z0-9 ]*$/, message: "may only contain letters, numbers and spaces")
    |> unique_constraint(:name, name: "tags_name_indexed_index")
  end

  defp set_name_indexed(changeset) do
    if is_nil(changeset.data.inserted_at) do
      indexed =
        (get_field(changeset, :name) || "")
        |> String.downcase
        |> String.replace(~r/[^a-z0-9]/, "")
        |> String.slice(0..9)
      force_change(changeset, :name_indexed, indexed)
    else
      changeset
    end
  end

  def get_sort_type(sort \\ "") do
    sort = String.to_atom(sort || "")
    if sort in @sorting_names, do: sort, else: :hot
  end

  def subscribed_tags(user_id \\ nil, blocked \\ false) do
    cond do
      user_id ->
        from c in Tag,
        left_join: s in UserSubscription, on: s.tag_id == c.id and s.user_id == ^user_id,
        where: not is_nil(s.user_id) and s.blocked == ^blocked,
        order_by: c.name
      :else   ->
        from t in Tag, where: t.default == true, order_by: t.name
    end
    |> Repo.all
  end

  def join_subscriptions(query, user_id) when is_integer(user_id) do
    from q in query,
      left_join: s in UserSubscription, on: s.tag_id == q.id and s.user_id == ^user_id,
      preload: [subscription: s]
  end
  def join_subscriptions(query, _), do: query

  def inc_subscription_count(category_ids, change \\ 1)
  def inc_subscription_count(category_ids, change) when is_integer(category_ids) do
    inc_subscription_count([category_ids], change)
  end
  def inc_subscription_count(category_ids, change) when is_binary(category_ids) do
    inc_subscription_count([String.to_integer(category_ids)], change)
  end
  def inc_subscription_count(category_ids, change) do
    from(c in Tag, where: c.id in ^category_ids, update: [inc: [subscribers: ^change]])
    |> Repo.update_all([])
  end
end
