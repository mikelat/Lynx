defmodule Lynx.Comment do
  use Lynx.Web, :model

  schema "comments" do
    field :text, :string
    field :text_cached, :string
    field :ip_address, :string
    field :depth, :integer, default: 0
    field :deleted, :boolean, default: false
    field :votes_count, :integer, default: 0
    field :score_up, :float, default: 0.0
    field :score_down, :float, default: 0.0
    field :score, :integer, default: 0
    field :hot, :float, default: 0.0
    belongs_to :comment, Comment
    belongs_to :link, Link
    belongs_to :user, User

    has_many :votes, Vote
    has_one :vote, Vote

    timestamps()
  end

  @edit_time_limit 2 # hours
  @delete_time_limit 2 # hours

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(data, params \\ %{}, _opts \\ []) do
    case is_nil(data.inserted_at) do
      true  -> cast(data, params, ~w(text link_id comment_id))
      false -> cast(data, params, ~w(text))
    end
    |> validate_required([:text])
    |> validate_length(:text, min: 1, max: 10000)
    |> validate_content_id
    |> set_cache_text_markdown
    |> set_parent_comment_params
  end

  def with_joins(query, opts \\ []) do
    query = from c in query, join: u in assoc(c, :user), preload: [user: u]

    if opts[:user] do
      from l in query,
      left_join: v in Vote, on: l.id == v.comment_id and v.user_id == ^opts[:user],
      preload: [vote: v]
    else
      query
    end
  end

  def by_parent(query, link_id, opts \\ []) do
    comments = Repo.all from c in with_joins(query, user: opts[:user]),
                        where: c.link_id == ^link_id,
                        order_by: [desc: c.score]
    sort_by_key(comments, :comment_id)
  end

  def can_edit?(comment, current_user) do
    inserted_at = Timex.to_datetime(comment.inserted_at)
    cutoff_at = Timex.shift(Timex.now, hours: @edit_time_limit * -1)
    current_user.admin || (Timex.compare(inserted_at, cutoff_at) == 1 && comment.user_id == current_user.id)
  end

  def can_delete?(comment, current_user) do
    inserted_at = Timex.to_datetime(comment.inserted_at)
    cutoff_at = Timex.shift(Timex.now, hours: @delete_time_limit * -1)
    current_user.admin || (Timex.compare(inserted_at, cutoff_at) == 1 && comment.user_id == current_user.id)
  end

  def insert(changeset) do
    case Repo.insert(changeset) do
      {:ok, comment} ->
        Link.change_comment_count(comment.link_id)

        from(u in User, where: u.id == ^comment.user_id, update: [inc: [comment_count: 1]])
        |> Repo.update_all([])

        %Vote{user_id: comment.user_id,
              ip_address: comment.ip_address,
              weight_up: 1.0,
              weight_down: 1.0,
              comment_id: comment.id}
        |> change(change: 1)
        |> Vote.change!

        {:ok, comment}
      {:error, changeset} -> {:error, changeset}
    end
  end

  def insert!(changeset) do
    case insert(changeset) do
      {:ok, data} -> data
      {:error, changeset} ->
        raise Ecto.InvalidChangesetError, action: :insert, changeset: changeset
    end
  end

  def delete!(model) do
    if model.deleted do
      model
    else
      Link.change_comment_count(model.link_id, -1)
      model
      |> change(%{deleted: true})
      |> Repo.update!
    end
  end

  defp validate_content_id(changeset) do
    comment_id = get_change(changeset, :comment_id)
    link_id = get_change(changeset, :link_id)
    # Only validates if error free and inserting record
    cond do
      !changeset.valid? || changeset.data.inserted_at ->
        changeset
      comment_id && !Repo.get(Comment, comment_id) ->
        add_error(changeset, :comment_id, "is invalid")
      link_id && !Repo.get(Link, link_id) ->
        add_error(changeset, :link_id, "is invalid")
      !link_id && !comment_id ->
        add_error(changeset, :comment, "is invalid")
      :else -> changeset
    end
  end

  defp set_parent_comment_params(changeset) do
    if changeset.valid? && get_change(changeset, :comment_id) do
      comment = Repo.get(Comment, get_change(changeset, :comment_id))
      changeset
      |> force_change(:depth, comment.depth + 1)
      |> force_change(:link_id, comment.link_id)
    else
      changeset
    end
  end

  defp set_cache_text_markdown(changeset) do
    if changeset.valid? do
      text_cached = MarkdownParser.convert(get_field(changeset, :text))
      force_change(changeset, :text_cached, text_cached)
    else
      changeset
    end
  end
end
