defmodule Lynx.LinkRead do
  use Lynx.Web, :model

  schema "links_read" do
    belongs_to :link, Link
    belongs_to :user, User

    timestamps updated_at: false
  end
end

defmodule Lynx.Link do
  use Lynx.Web, :model
  require Logger

  schema "links" do
    field :title, :string
    field :slug, :string
    field :slug_id, :string
    field :url, :string
    field :domain, :string
    field :ip_address, :string
    field :text, :string
    field :text_cached, :string
    field :thumbnail, :string
    field :thumbnail_failed, :boolean, default: false
    field :thumbnail_generating, :boolean, default: false
    field :spoiler, :boolean, default: false
    field :comments_count, :integer, default: 0
    field :votes_count, :integer, default: 0
    field :content_type, :integer, default: 0
    field :score_up, :float, default: 0.0
    field :score_down, :float, default: 0.0
    field :score, :integer, default: 0
    field :hot, :float, default: 0.0
    field :deleted, :integer, default: 0
    field :deleted_date, Timex.Ecto.DateTime
    field :nsfw, :boolean, default: false

    field :tag_list, :string, virtual: true

    many_to_many :tags, Tag, join_through: "links_tags"
    belongs_to :user, User

    has_many :votes, Vote
    has_one :vote, Vote

    has_many :reads, LinkRead
    has_one :read, LinkRead

    timestamps()
  end

  @required_fields ~w(title tag_list)a
  @optional_fields ~w(text url nsfw)a
  @max_slug_length 40
  @banned_thumbnails ~r/(localhost|::1|127\.0\.[0-1]\.[0-1]|192\.168\.[0-9]{1,3}\.[0-9]{1,3}+)$/i
  @types [
    [type: :url, icon: nil, class: ""],
    [type: :text, icon: nil, class: "text"],
    [type: :image, icon: "fa-camera", class: "image"],
    [type: :video, icon: "fa-video-camera", class: "video"],
    [type: :audio, icon: "fa-volume-up", class: "audio"]
  ]
  def types, do: @types
  @media [
    "youtube.com": [
      type: :video,
      regex: ~r/youtube\.com\/watch\?v=([A-Za-z0-9\-_]+)/i,
      embed: "<iframe class=\"iframe-embed\" width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/<!-- ID -->?autoplay=1\" allowfullscreen></iframe>",
      thumb: "https://img.youtube.com/vi/<!-- ID -->/mqdefault.jpg"
    ],
    "youtu.be": [
      type: :video,
      regex: ~r/youtu\.be\/watch\?v=([A-Za-z0-9\-_]+)/i,
      embed: "<iframe class=\"iframe-embed\" width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/<!-- ID -->?autoplay=1\" allowfullscreen></iframe>",
      thumb: "https://img.youtube.com/vi/<!-- ID -->/mqdefault.jpg"
    ],
    "soundcloud.com": [
      type: :audio,
      regex: ~r/(soundcloud.com\/.+)/i,
      embed: "<iframe width=\"100%\" height=\"166\" scrolling=\"no\" frameborder=\"0\" src=\"https://w.soundcloud.com/player/?url=<!-- ID -->&amp;color=ff5500&amp;auto_play=true&amp;hide_related=false&amp;show_comments=false&amp;show_user=false&amp;show_reposts=false\"></iframe>"
    ],
  ]
  def media, do: @media
  @image_media [
    domains: ["i.imgur.com"],
    exts: ~r/\.(jpg|jpeg|gif|png)$/
  ]
  def image_media, do: @image_media
  @unread_days 10
  @hashids_alpha to_string(Enum.to_list(?a..?z) ++ Enum.to_list(?0..?9))
  @hashids Hashids.new(salt: config(:hashids_key), min_len: 5, alphabet: @hashids_alpha)

  def content_id_by_type(type), do: Enum.find_index(@types, fn(x) -> x[:type] == type end)
  def content_type(link), do: Enum.at(@types, link.content_type)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model \\ %Link{}, params \\ %{}, opts \\ []) do
    changeset = cast(model, params, @required_fields ++ @optional_fields)
    if is_nil(changeset.data.inserted_at) && !opts[:force] && opts[:ip_address] do
      Lynx.Recaptcha.validate(changeset, opts[:ip_address], opts[:captcha])
    else
      changeset
    end
    |> validate_required(@required_fields)
    |> update_change(:title, &String.strip/1)
    |> validate_length(:title, min: 1, max: 255)
    |> validate_length(:text, min: 5, max: 25000)
    |> validate_length(:url, max: 2000)
    |> validate_format(:tag_list, ~r/[A-Za-z0-9,]/i)
    |> validate_tags
    |> set_text_or_url
    |> encode_url
    |> validate_change(:url, &validate_url/2)
    |> set_content_type
    |> set_cache_text_markdown
  end

  def update_thumbnail(link) do
    if link.url do
      spawn(Link, :generate_thumbnail, [link])
      |> Process.register(String.to_atom("lynx_link_#{link.id}"))
    end

    File.rm("#{config(:thumbnails_path)}#{link.thumbnail}.png")
  end

  defp set_cache_text_markdown(changeset) do
    if changeset.valid? && get_change(changeset, :text) do
      text_cached = MarkdownParser.convert(get_field(changeset, :text))
      force_change(changeset, :text_cached, text_cached)
    else
      changeset
    end
  end

  defp validate_url(atom, value) do
    result =
      case URI.parse(value) do
        %URI{scheme: nil} -> :error
        %URI{host: nil}   -> :error
        _                 -> :ok
      end
    if result == :error do
      [{atom, {"is invalid", []}}]
    else
      []
    end
  end

  defp validate_tags(changeset) do
    if get_field(changeset, :tag_list) != nil do
      inputted_tags =
        changeset
        |> get_field(:tag_list)
        |> String.split(",")
      found_tags = Repo.all(from t in Tag, where: t.name_indexed in ^inputted_tags)
      cond do
        Enum.count(inputted_tags) > 5 ->
          add_error(changeset, :base, "Too many tags were entered.")
        Enum.count(found_tags) != Enum.count(inputted_tags) ->
          add_error(changeset, :base, "Some of the tags entered do not exist.")
        :else ->
          tags = Enum.map(found_tags, &Ecto.Changeset.change/1)
          put_assoc(changeset, :tags, tags)
      end
    else
      changeset
    end
  end

  defp encode_url(changeset) do
    if changeset.valid? && get_change(changeset, :url) do
      # This fixes any encoding issues (like adding html entities in the submission)
      url =
        get_field(changeset, :url)
        |> URI.decode
        |> URI.encode
      # Add protocol if none was specified
      url = if url =~ ~r/^[a-z0-9\+\.\-]+\:\/\/.*/i, do: url, else: "http://" <> url
      force_change(changeset, :url, url)
    else
      changeset
    end
  end

  defp set_text_or_url(changeset) do
    cond do
      !get_field(changeset, :url) && !get_field(changeset, :text) ->
        add_error(changeset, :url, "or text is required")
      get_field(changeset, :url) && get_field(changeset, :text) ->
        add_error(changeset, :url, "and text together are not allowed")
      :else ->
        changeset
    end
  end

  def with_joins(query, opts \\ []) do
    query = from link in query,
            join: tags in assoc(link, :tags),
            join: user in assoc(link, :user),
            preload: [user: user, tags: tags],
            order_by: [desc: tags.subscribers, asc: tags.name]

    query =
      if !opts[:user] || !opts[:user].admin do
        from link in query, where: link.deleted != 2
      else
        query
      end
    if opts[:user] && opts[:user].id do
      from link in query,
      left_join: v in Vote, on: link.id == v.link_id and v.user_id == ^opts[:user].id,

      left_join: lr in LinkRead, on: (link.id == lr.link_id and lr.user_id == ^opts[:user].id
            and link.inserted_at > ^Timex.shift(Timex.now, days: @unread_days * -1))
            or (link.inserted_at <= ^Timex.shift(Timex.now, days:  @unread_days * -1) and lr.link_id == 0),
      #left_join: lr in LinkRead, on: link.id == lr.link_id and lr.user_id == ^opts[:user].id,
      preload: [vote: v, read: lr]
    else
      query
    end
  end

  def list_with_joins(query, opts \\ []) do
    import Ecto.Query, only: [from: 1, from: 2, subquery: 1, order_by: 3]
    # Query variable expects that the base query comes from Link and joins the associated Tags table

    # We cannot use offset and limit on the default query because tags is a one to many
    # relationship, which does not return the accurate amount of rows. So instead, we specifically
    # fetch the IDs in one query so we get the correct amount, and then afterwards we fetch just the
    # actual data. Worried it may not scale incredibly well but I'll fix it when it becomes an issue.
    count_query = from [link, tags] in query,
                  join: user in assoc(link, :user),
                  where: not is_nil(link.slug) and link.deleted == 0,
                  select: %{id: link.id, hot: link.hot, inserted_at: link.inserted_at, score: link.score},
                  distinct: true

    # TODO: proper filtering, user driven
    count_query =
      case opts[:sort] do
        :top ->
          days =
            case opts[:params]["t"] do
              "day"   -> 1
              "3days" -> 3
              "week"  -> 7
              "month" -> 30
              "year"  -> 365
              _       -> 0
            end
          if days > 0 do
            from link in count_query, where: link.inserted_at >= ^Timex.shift(Timex.now, days: days * -1)
              and link.score >= 0
          else
            from link in count_query, where: link.score >= 0
          end
        :hot ->
          from link in count_query, where: link.score >= 0
        :unread ->
          if !opts[:user], do: raise "Cannot get unread links unless user is passed"
          from link in count_query,
            left_join: lr in LinkRead, on: link.id == lr.link_id and lr.user_id == ^opts[:user],
            where: is_nil(lr.id) and link.inserted_at > ^Timex.shift(Timex.now, days: @unread_days * -1)
              and link.score >= 0
        _ -> count_query
      end
      |> list_with_joins_order(opts[:sort])

    # Count only is used mostly for getting unread counters
    if opts[:count_only] do
      from link in subquery(count_query), select: count(link.id)
    else
      link_ids =
        # After x date
        if opts[:sort] in [:new, :unread] do
          slug = slug_to_id(opts[:params]["start"])
          start_link = if slug, do: Repo.get(Link, slug), else: nil
          if start_link do
            from link in subquery(from link in count_query, where: link.inserted_at <= ^start_link.inserted_at)
          else
            from link in subquery(count_query)
          end
        # Pagination
        else
          from link in subquery(count_query), offset: ^(to_integer(opts[:params]["page"]) * 50)
        end
        |> Ecto.Query.limit(^((opts[:limit] || 50) + 1))
        |> Repo.all
        |> Enum.map(fn(x) -> x.id end)

      query = from link in Link,
              join: tags in assoc(link, :tags),
              join: user in assoc(link, :user),
              preload: [user: user, tags: tags],
              where: link.id in ^(link_ids || [0])

      if opts[:user] do
        from link in query,
        left_join: lr in LinkRead, on: (link.id == lr.link_id and lr.user_id == ^opts[:user]
              and link.inserted_at > ^Timex.shift(Timex.now, days: @unread_days * -1))
              or (link.inserted_at <= ^Timex.shift(Timex.now, days:  @unread_days * -1) and lr.link_id == 0),
        left_join: v in Vote, on: link.id == v.link_id and v.user_id == ^opts[:user],
        preload: [read: lr, vote: v]
      else
        query
      end
      |> list_with_joins_order(opts[:sort])
      |> order_by([link, tags], desc: tags.subscribers, asc: tags.name)
    end
  end

  defp list_with_joins_order(query, sort) do
    case sort do
      x when x == :new or x == :unread ->
        from [link, tags] in query, order_by: [desc: link.inserted_at, desc: link.id]
      :top ->
        from [link, tags] in query, order_by: [desc: link.score, desc: link.inserted_at, desc: link.id]
      :hot ->
        from [link, tags] in query, order_by: [desc: link.hot, desc: link.score, desc: link.inserted_at, desc: link.id]
      _ -> query
    end
  end

  def slug_to_id(slug) when is_binary(slug) do
    slug
    |> String.split("-")
    |> List.last
    |> slug_id_to_id
  end
  def slug_to_id(_), do: nil

  def slug_id_to_id(id) do
    {:ok, id} = Hashids.decode(@hashids, id)
    List.last(id)
  end

  def slugify(model) do
    slug =
      model.title
      |> String.downcase
      |> String.replace(~r/[^a-z0-9]+/, "-")
      |> String.replace(~r/-+/, "-")
      |> String.strip(?-)
      |> String.slice(0..@max_slug_length)
      |> String.strip(?-)

    slug =
      case String.length(slug) do
        # Attempt to remove any partial words by the dash seperator
        x when x >= @max_slug_length - 1 ->
          shortened_slug = String.replace(slug, ~r/([a-z0-9\-]+)\-[a-z0-9]+$/, "\\1")
          if String.length(shortened_slug) > 30, do: shortened_slug, else: slug
        # Couldn't generate slug, so make a random slug hash
        x when x == 0 ->
          :crypto.hash(:sha, to_string(model.url) <> to_string(model.text))
          |> Base.encode16 |> String.slice(0..10) |> String.downcase
        _ -> slug
      end

    {"#{slug}", Hashids.encode(@hashids, model.id)}
  end

  def insert(changeset, opts \\ []) do
    case Repo.insert(changeset, opts) do
      {:ok, link} ->
        {slug, slug_id} = slugify(link)

        gen_thumb =
          content_type(link)[:type] != :text
          && !(link.domain =~ @banned_thumbnails)
          && config(:generate_thumbnails)

        link =
          link
          |> change(slug: "#{slug}-#{slug_id}", slug_id: slug_id, thumbnail_generating: gen_thumb)
          |> Repo.update!

        if gen_thumb do
          spawn(Link, :generate_thumbnail, [link])
          |> Process.register(String.to_atom("lynx_link_#{link.id}"))
        end

        from(u in User, where: u.id == ^link.user_id, update: [inc: [link_count: 1]])
        |> Repo.update_all([])

        %Vote{user_id: link.user_id,
              ip_address: link.ip_address,
              weight_up: 1.0,
              weight_down: 1.0,
              link_id: link.id}
        |> change(change: 1)
        |> Vote.change!

        {:ok, link}
      {:error, changeset} ->
        {:error, changeset}
    end
  end

  def insert!(changeset, opts \\ []) do
    case insert(changeset, opts) do
      {:ok, model} -> model
      {:error, changeset} ->
        raise Ecto.InvalidChangesetError, action: :insert, changeset: changeset
    end
  end

  def change_comment_count(link_id, count \\ 1) do
    from(l in Link, where: l.id == ^link_id, update: [inc: [comments_count: ^count]])
    |> Repo.update_all([])
  end

  def is_unread(link) do
    Ecto.assoc_loaded?(link.read) && is_nil(link.read)
  end

  def generate_thumbnail(link) do
    link =
      link
      |> change(thumbnail: nil, thumbnail_failed: true, thumbnail_generating: true)
      |> Repo.update!

    File.mkdir(config(:thumbnails_path))
    file_name = "#{link.slug_id}-#{random_string(5)}"
    tmp_file = "#{config(:tmp_path)}#{link.id}.png"
    file = "#{config(:thumbnails_path)}#{file_name}.png"

    try do
      # Try to find suitable image candidate for thumbnail
      asset_url =
        if link.url =~ @image_media[:exts] do # url is an image ext
          link.url
        else # take og:image meta param from page itself
          case System.cmd("timeout", ["4", "curl", link.url, "-L", "-s", "--compressed"]) do
            {html, 0} ->
              try do
                Floki.find(html, "meta[property='og:image']")
                |> Floki.attribute("content")
                |> List.first
              rescue
                _ -> nil # Floki couldn't parse it
              end
            _ -> nil
          end
        end

      # If suitable candidate is found, download it and parse it
      asset_parse_result =
        if asset_url != nil do
          case System.cmd("timeout", ["4", "curl", asset_url, "-L", "-s", "-o", tmp_file]) do
            {_, 0} -> thumbnail_parse_image(tmp_file)
            _      -> false
          end
        end

      # Candidate above did not seem to successfully parse, we take a screenshot of the page instead
      asset_parse_result =
        if !asset_parse_result do
          case System.cmd("timeout", ~w(8 cutycapt --min-width=1280 --min-height=720 --plugins=off
                 --private-browsing=on --javascript=off) ++ ["--url=#{link.url}", "--out=#{tmp_file}"],
                 env: [{"DISPLAY", ":0"}, {"http_proxy", System.get_env("LYNX_PROXY")}]) do
            {_, 0} -> thumbnail_parse_image(tmp_file)
            _      -> false
          end
        else
          asset_parse_result
        end

      if asset_parse_result do
        # Elixir's File.rename doesn't work cross device, we use the OS to move instead
        System.cmd("mv", [tmp_file, file])
        unless File.exists?(file), do: raise "Could not move file"
        change(link, thumbnail: file_name, thumbnail_failed: false, thumbnail_generating: false)
      else
        change(link, thumbnail: nil, thumbnail_failed: true, thumbnail_generating: false)
      end
      |> Repo.update!
    after
      File.rm(tmp_file)
    rescue
      e ->
        link
        |> change(thumbnail: nil, thumbnail_failed: true, thumbnail_generating: false)
        |> Repo.update!
        raise e
    end
  end

  defp thumbnail_parse_image(tmp_file) do
    # There's a lot that can go wrong with parsing the image (corruption, failed downloads, etc)
    # So we rescue from any errors in this part and simply return a success/failure boolean state
    # I'm debating down the road rescuing from common types of errors instead
    try do
      [w, h] =
        "identify -format \"%w %h\" #{tmp_file}"
        |> String.to_char_list
        |> :os.cmd
        |> to_string
        |> String.split(" ")
        |> Enum.map(&(String.to_integer(&1)))

      alignment = if w * 1.2 > h, do: "center", else: "north"

      System.cmd("convert", ~w(#{tmp_file}[0] -thumbnail 112x63^ -background none
                  -gravity #{alignment} -extent 112x63 #{tmp_file}))
      System.cmd("pngquant", ~w(-o #{tmp_file} --force --quality=30-50 #{tmp_file}))

      if !File.exists?(tmp_file), do: raise "Thumbnail not generated"
      {_, 0} = System.cmd("identify", [tmp_file]) # verifies if valid image
      true
    rescue
      _ -> false
    end
  end

  defp set_content_type(changeset) do
    cond do
      # changeset is invalid, or the url/text wasn't changed
      !changeset.valid? || (!get_change(changeset, :url) && !get_change(changeset, :text)) ->
        changeset
      is_nil(get_field(changeset, :url)) ->
        force_change(changeset, :content_type, content_id_by_type(:text))
      :else ->
        url = get_field(changeset, :url)
        {url, changeset} =
          if !(get_field(changeset, :url) =~ ~r/^[a-zA-Z]+:.+/) do
            {"http://" <> url, force_change(changeset, :url, "http://" <> url)}
          else
            {url, changeset}
          end

        domain =
          URI.parse(url).host
          |> String.downcase
          |> String.replace(~r/(?:www.)?(.*)$/, "\\1")

        content_type = cond do
          domain in @image_media[:domains] ->
            content_id_by_type(:image)
          !is_nil(@media[String.to_atom(domain)]) && url =~ @media[String.to_atom(domain)][:regex] ->
            content_id_by_type(@media[String.to_atom(domain)][:type])
          :else ->
            content_id_by_type(:url)
        end
        changeset
        |> force_change(:domain, domain)
        |> force_change(:content_type, content_type)
    end
  end
end