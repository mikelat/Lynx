defmodule Lynx.User do
  use Lynx.Web, :model

  schema "users" do
    field :name, :string
    field :name_lower, :string
    field :password, :string, virtual: true
    field :password_encrypted, :string
    field :email, :string
    field :ip_address, :string
    field :profile, :string
    field :profile_cached, :string
    field :admin, :boolean, default: false
    field :show_nsfw, :boolean, default: false
    field :unread_enable, :boolean, default: true
    field :unread_scroll, :boolean, default: true
    field :unread_click, :boolean, default: true
    field :unread_comment, :boolean, default: true
    field :comment_score, :float, default: 0.0
    field :link_score, :float, default: 0.0
    field :comment_count, :integer, default: 0
    field :link_count, :integer, default: 0
    field :weight_up, :float, default: 1.0
    field :weight_down, :float, default: 1.0
    field :unread_count, :integer, default: 0
    field :unread_date, Timex.Ecto.DateTime
    field :banned, :boolean, default: false

    #has_many :userauths, "user_auths"

    timestamps()
  end

  @fields ~w(email show_nsfw unread_enable unread_scroll unread_click unread_comment profile password)a

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(data, params \\ %{}, opts \\ []) do
    changeset =
      case is_nil(data.inserted_at) do
        true  -> cast(data, params, ~w(name password email)a)
                 |> validate_required([:name, :password])
        false -> cast(data, params, [], @fields)
      end
    if is_nil(changeset.data.inserted_at) && !opts[:force] && opts[:ip_address] do
      Lynx.Recaptcha.validate(changeset, opts[:ip_address], opts[:captcha])
    else
      changeset
    end
    |> validate_length(:name, min: 3, max: 25)
    |> validate_format(:name, ~r/[a-z0-9\-]/i)
    |> validate_length(:email, max: 255)
    |> validate_format(:email, ~r/^[a-zA-Z0-9_\.\+\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/)
    |> validate_length(:profile, max: 25000)
    |> validate_length(:password, min: 6, max: 420)
    |> validate_change_password
    |> set_password
    |> set_name_lower
    |> set_cache_text_markdown
    |> unique_constraint(:name, name: "users_name_lower_index")
  end

  def can_edit?(user, current_user) do
    !is_nil(user) && (current_user.admin || user.id == current_user.id)
  end

  def authenticate(user, password) do
    if user do
      Comeonin.Bcrypt.checkpw(to_string(password), user.password_encrypted)
    else
      false
    end
  end

  def validate_change_password(changeset) do
    password = get_change(changeset, :password)
    current_password = changeset.params["current_password"]
    cond do
      # Only validates if: is an existing user and form is valid, also having the proper fields
      is_nil(get_field(changeset, :inserted_at)) || !changeset.valid? ||
      !(Map.has_key?(changeset.params, "password") && Map.has_key?(changeset.params, "current_password")) ->
        changeset
      is_nil(current_password) ->
        add_error(changeset, :current_password, "was left blank")
      is_nil(password) ->
        add_error(changeset, :password, "was left blank")
      !Comeonin.Bcrypt.checkpw(current_password, get_field(changeset, :password_encrypted, "")) ->
        add_error(changeset, :current_password, "is incorrect")
      :else -> changeset
    end
  end

  def rebuild_unread(user) do
    if user && user.id do
      user_tags = UserSubscription.get_user_subscriptions(user.id)
      count =
        from(link in Link, join: tags in assoc(link, :tags), where: tags.id in ^(user_tags[:subscribed] || [0]))
        |> Link.list_with_joins(user: user.id, sort: :unread, limit: 250, count_only: true)
        |> Repo.one

      user
      |> change(unread_count: count, unread_date: Timex.shift(Timex.now, minutes: 5))
      |> Repo.update!
    else
      user
    end
  end

  def change_unread_count(user_id, count \\ 1) do
    from(user in User, where: user.id == ^user_id, update: [inc: [unread_count: ^count]])
    |> Repo.update_all([])
  end

  def insert(changeset) do
    case Repo.insert(changeset) do
      {:ok, user} ->
        default_tag_ids =
          from(t in Tag, where: t.default == true, select: t.id)
          |> Repo.all
        # Subscribe to default categories
        Enum.each(default_tag_ids, fn(x) ->
          Repo.insert(%UserSubscription{user_id: user.id, tag_id: x})
        end)
        # Increment counter for the categories
        Tag.inc_subscription_count(default_tag_ids)
        {:ok, user}
      {:error, changeset} -> {:error, changeset}
    end
  end

  def insert!(changeset) do
    case insert(changeset) do
      {:ok, model} -> model
      {:error, changeset} ->
        raise Ecto.InvalidChangesetError, action: :insert, changeset: changeset
    end
  end

  defp set_cache_text_markdown(changeset) do
    if changeset.valid? && get_change(changeset, :profile) do
      text_cached = MarkdownParser.convert(get_field(changeset, :profile))
      force_change(changeset, :profile_cached, text_cached)
    else
      changeset
    end
  end

  defp set_name_lower(changeset) do
    if get_change(changeset, :name) do
      force_change(changeset, :name_lower, String.downcase(get_field(changeset, :name) || ""))
    else
      changeset
    end
  end

  defp set_password(changeset) do
    password = get_change(changeset, :password)
    if changeset.valid? && !is_nil(password) do
      force_change(changeset, :password_encrypted, Comeonin.Bcrypt.hashpwsalt(password))
    else
      changeset
    end
  end
end
