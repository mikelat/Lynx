defmodule Lynx.Vote do
  use Lynx.Web, :model
  import Lynx.Session

  schema "votes" do
    field :weight_up, :float, default: 0.0
    field :weight_down, :float, default: 0.0
    field :change, :integer, default: 0
    #field :referer, :string
    field :ip_address, :string

    belongs_to :comment, Comment
    belongs_to :link, Link
    belongs_to :user, User

    timestamps()
  end

  @valid_content_types %{link: Link, comment: Comment}
  def valid_content_types, do: @valid_content_types

  def change!(changeset, opts \\ []) do
    model = Repo.insert_or_update!(changeset, opts)
    if get_field(changeset, :change) != changeset.data.change do
      {score_up, score_down} =
        case model.change do
          1  -> {get_field(changeset, :weight_up), 0}
          #-1 -> {0, get_field(changeset, :weight_down)}
          _  -> {0, 0}
        end
      {score_up, score_down} =
        case changeset.data.change do
          1  -> {score_up - get_field(changeset, :weight_up), score_down}
          #-1 -> {score_up, score_down - get_field(changeset, :weight_down)}
          _  -> {score_up, score_down}
        end

      if score_up != 0 || score_down != 0 do
        type = content_type(changeset)
        if !@valid_content_types[type], do: raise "Content type not found for vote"
        id = get_field(changeset, String.to_atom("#{type}_id"))
        hot = Vote.calculate_hot(Repo.get(@valid_content_types[type], id), score_up + (score_down * -1))

        posted_by_user_id =
          from(x in @valid_content_types[type], where: x.id == ^id, select: x.user_id)
          |> Repo.one!(opts)

        user_inc = [{String.to_atom("#{type}_score"), round(score_up + (score_down * -1))}]

        from(user in User, where: user.id == ^posted_by_user_id, update: [inc: ^user_inc])
        |> Repo.update_all([], opts)


        from(x in @valid_content_types[type], where: x.id == ^id,
          update: [inc: [score: ^round(score_up + (score_down * -1)),
                         score_up: ^score_up,
                         score_down: ^score_down,
                         votes_count: ^if(model.inserted_at == nil, do: 1, else: 0)],
                   set: [hot: ^hot]])
        |> Repo.update_all([], opts)
      end
    end
  end

  def content_type(changeset) do
    Map.keys(@valid_content_types)
    |> Enum.reject(fn(x) -> get_field(changeset, String.to_atom("#{x}_id")) == nil end)
    |> List.first
  end

  def changeset_defaults(vote, conn) do
    change(vote, %{ip_address: remote_ip(conn),
                   user_id: current_user(conn).id,
                   weight_up: current_user(conn).weight_up,
                   weight_down: current_user(conn).weight_down})
  end

  def calculate_hot(model, change \\ 0) do
    if model.score < -10 || Timex.diff(Timex.now, model.inserted_at, :days) >= 10 do
      0.0
    else
      hours_diff = Timex.diff(Timex.now, model.inserted_at, :hours)
      (model.score + change) / :math.pow((hours_diff + 2), 1.5)
    end
  end
end
