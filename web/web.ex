defmodule Lynx.Web do
  @moduledoc """
  A module that keeps using definitions for controllers,
  views and so on.

  This can be used in your application as:

      use Lynx.Web, :controller
      use Lynx.Web, :view

  The definitions below will be executed for every view,
  controller, etc, so keep them short and clean, focused
  on imports, uses and aliases.

  Do NOT define functions inside the quoted expressions
  below.
  """

  def model do
    quote do
      use Ecto.Schema
      use Timex
      use Timex.Ecto.Timestamps

      import Ecto
      import Ecto.Changeset
      import Ecto.Query, only: [from: 1, from: 2]
      import Lynx.Utils

      alias Lynx.Repo
      alias Lynx.MarkdownParser

      unquote(lynx_models())
    end
  end

  def controller do
    quote do
      use Phoenix.Controller
      use Timex

      import Ecto
      import Ecto.Schema
      import Ecto.Query, only: [from: 1, from: 2]
      import Phoenix.View, only: [render_to_string: 3]

      import Lynx.Router.Helpers
      import Lynx.Router.Plugs
      import Lynx.Utils
      import Lynx.Session

      alias Lynx.Repo
      alias Lynx.Exceptions
      alias Ecto.Query
      alias Ecto.Changeset

      unquote(lynx_models())
    end
  end

  def view do
    quote do
      use Phoenix.View, root: "web/templates"
      use Phoenix.HTML

      # Import convenience functions from controllers
      import Phoenix.Controller, only: [get_csrf_token: 0, get_flash: 2, view_module: 1]
      import Lynx.{Router.Helpers, AppView, Session, Utils}
      alias Lynx.MarkdownParser

      unquote(lynx_models())
    end
  end

  def router do
    quote do
      use Phoenix.Router
      import Lynx.Router.Plugs
    end
  end

  def channel do
    quote do
      use Phoenix.Channel

      alias Lynx.Repo
      import Ecto
      import Ecto.Schema
      import Ecto.Query, only: [from: 1, from: 2]
    end
  end

  defp lynx_models do
    quote do
      alias Lynx.{Comment, Link, LinkRead, Tag, User, UserSubscription, Vote, Rss}
    end
  end

  @doc """
  When used, dispatch to the appropriate controller/view/etc.
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
