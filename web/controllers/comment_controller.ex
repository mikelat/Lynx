defmodule Lynx.CommentController do
  use Lynx.Web, :controller
  plug :require_signed_in when action in [:create, :update, :delete]
  plug :scrub_params, "comment" when action in [:create, :update]

  def create(conn, %{"comment" => params}) do
    changeset =
      Comment.changeset(%Comment{}, params)
      |> Changeset.change(ip_address: remote_ip(conn), user_id: current_user(conn).id)

    case Comment.insert(changeset) do
      {:ok, comment} ->
        link = Repo.get(Link.with_joins(Link), comment.link_id)
        redirect_json(conn, link_path(conn, :show, link.slug) <> "#comment_" <> to_string(comment.id))
      {:error, changeset} ->
        json conn, %{success: false, errors: changeset_errors(changeset)}
    end
  end

  def update(conn, %{"id" => id, "comment" => params}) do
    comment = Repo.get!(Comment, id)
    unless Comment.can_edit?(comment, current_user(conn)), do: raise Lynx.NoPermission

    case Repo.update(Comment.changeset(comment, params)) do
      {:ok, comment} ->
        json(conn, %{success: true, text_cached: comment.text_cached, text: comment.text})
      {:error, changeset} ->
        json conn, %{success: false, errors: changeset_errors(changeset)}
    end
  end

  def delete(conn, %{"id" => id}) do
    comment = Repo.get!(Comment.with_joins(Comment), id)
    unless Comment.can_delete?(comment, current_user(conn)), do: raise Lynx.NoPermission

    comment = Comment.delete!(comment)

    # Return deleted comment to embed
    comment_html = Phoenix.View.render(Lynx.LinkView, "_comment.html", conn: conn, comment: comment)
    json(conn, %{success: true, comment: Phoenix.HTML.safe_to_string(comment_html)})
  end
end
