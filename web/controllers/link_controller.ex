defmodule Lynx.LinkController do
  use Lynx.Web, :controller
  plug :require_signed_in when action in [:new, :create]
  plug :require_admin when action in [:edit, :update, :delete]
  plug :scrub_params, "link" when action in [:create, :update]
  plug :strip_unused_input when action in [:create, :update]

  def index(conn, params) do
    # The router defines the sorting attributes manually, so if more sort types are added you must
    # add them to the router as well
    sort =
      conn.path_info
      |> List.last
      |> Tag.get_sort_type
    # Index
    if sort == :unread && !current_user(conn).id do
      require_signed_in(conn)
    else
      conn = put_private(conn, :current_user, User.rebuild_unread(current_user(conn)))
      links =
        cond do
          # Get current users subscriptions only
          current_user(conn).id ->
            user_tags = UserSubscription.get_user_subscriptions(current_user(conn).id)
            from link in Link, join: tags in assoc(link, :tags),
            # Excludes blocked tags
            where: fragment("not exists(
              select * from users_subscriptions s
              left join links_tags lt on lt.tag_id=s.tag_id
              where lt.link_id = ? and s.user_id = ? and s.blocked = true
              )", link.id, ^current_user(conn).id)
              and tags.id in ^(user_tags[:subscribed] || [0])
          # Not logged in, only get default tags
          :else ->
            from link in Link, join: tags in assoc(link, :tags), where: tags.default == true
        end
        |> Link.list_with_joins(user: current_user(conn).id, sort: sort, params: params)
        |> Repo.all

      render(conn, links: links,
                   sort: sort,
                   tag: %Tag{},
                   unread_count: current_user(conn).unread_count,
                   mini_title: "#{sort}",
                   mini_url: "/",
                   title: "#{sort}")
    end
  end

  def new(conn, _params) do
    render(conn, title: "Submit", changeset: Link.changeset)
  end

  def create(conn, %{"link" => link_params}) do #, "g-recaptcha-response" => recaptcha
    changeset =
      Link.changeset(%Link{}, link_params, force: true) #captcha: recaptcha, ip_address: remote_ip(conn)
      |> Changeset.change(ip_address: remote_ip(conn), user_id: current_user(conn).id)

    case Link.insert(changeset) do
      {:ok, link} ->
        redirect_json(conn, link_path(conn, :show, link.slug))
      {:error, changeset} ->
        json(conn, %{success: false, errors: Map.drop(changeset_errors(changeset), [:tags])})
    end
  end

  def edit(conn, %{"id" => id}) do
    link =
      Link.with_joins(Link, user: current_user(conn))
      |> Repo.get!(Link.slug_to_id(id))

    render(conn, title: "edit link", mini_title: "edit", changeset: Link.changeset(link), link: link)
  end

  def update(conn, %{"id" => id, "link" => link_params}) do
    link =
      Link
      |> Link.with_joins(user: current_user(conn))
      |> Repo.get!(Link.slug_to_id(id))
    changeset = Link.changeset(link, link_params)
    deleted = String.to_integer(link_params["deleted"])
    changeset =
      if current_user(conn).admin and deleted != link.deleted and deleted do
        Changeset.change(changeset, deleted: deleted, deleted_date: Timex.now)
      else
        changeset
      end

    case Repo.update(changeset) do
      {:ok, link} ->
        if Changeset.get_change(changeset, :url) || link_params["force_thumbnail"] == "true" do
          Link.update_thumbnail(link)
        end
        conn
        |> put_flash(:success, "Link successfully updated.")
        |> redirect_json(link_path(conn, :show, link.slug))
      {:error, changeset} ->
        json(conn, %{success: false, errors: changeset_errors(changeset)})
    end
  end

  def show(conn, params) do
    link =
      Link.with_joins(Link, user: current_user(conn))
      |> Repo.get!(Link.slug_to_id(params["id"]))

    comments = Comment.by_parent(Comment, link.id, user: current_user(conn).id)

    # Automatically mark as read for current user
    {link, conn} =
      if Link.is_unread(link) && current_user(conn).unread_comment do
        Repo.insert_all(LinkRead, [[link_id: link.id, user_id: current_user(conn).id,
          inserted_at: Timex.now]], on_conflict: :nothing)
        # Fake the record and mark as read (or preload if it hasn't been loaded)
        conn =
          if link.score >= 0 do
            User.change_unread_count(current_user(conn).id, -1)
            put_private(conn, :current_user, %User{current_user(conn) | unread_count: current_user(conn).unread_count - 1})
          else
            conn
          end
        if Ecto.assoc_loaded?(link.read) && !is_nil(link.read) do
          {Map.put(link, :read, %LinkRead{link_id: link.id, user_id: current_user(conn).id}), conn}
        else
          {Repo.preload(link, :read), conn}
        end
      else
        {link, conn}
      end

    #Link.generate_thumbnail(link)

    render(conn, link: link,
                 comments: comments,
                 title: link.title,
                 mini_url: "",
                 mini_title: "comments")
  end

  def thumbnail(conn, %{"id" => id}) do
    link = Repo.get!(Link, Link.slug_to_id(id))
    thumbnail = unless link.thumbnail_generating, do: Lynx.LinkView.thumbnail_url(link)
    json(conn, %{thumbnail: thumbnail})
  end

  defp strip_unused_input(conn, _) do
    param = Map.get(conn.params, "link")
    param =
      if param["type"] == "1" do
        Map.delete(param, "url")
      else
        Map.delete(param, "text")
      end
    params = Map.put(conn.params, "link", param)
    %{conn | params: params}
  end
end
