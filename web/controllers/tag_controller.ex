defmodule Lynx.TagController do
  use Lynx.Web, :controller
  plug :require_signed_in when action in [:new, :create]
  plug :require_admin when action in [:edit, :update, :delete]
  plug :scrub_params, "tag" when action in [:create]

  def index(conn, _) do
    tags =
      (from t in Tag, order_by: [desc: t.subscribers, asc: t.name_indexed])
      |> Tag.join_subscriptions(current_user(conn).id)
      |> Repo.all

    conn
    |> assign(:title, "Tags")
    |> render("index.html", tags: tags)
  end

  def show(conn, params) do
    tag =
      from(tag in Tag, where: tag.name_indexed == ^params["id"])
      |> Tag.join_subscriptions(current_user(conn).id)
      |> Repo.one!

    sort = Tag.get_sort_type(params["sort"])

    if sort == :unread && !current_user(conn).id do
      require_signed_in(conn)
    else
      query = from link in Link, join: tags in assoc(link, :tags), where: tags.id == ^tag.id

      links =
        query
        |> Link.list_with_joins(user: current_user(conn).id, sort: sort, params: params)
        |> Repo.all

      count =
        if current_user(conn).id do
          query
          |> Link.list_with_joins(user: current_user(conn).id, sort: :unread, limit: 250,
               count_only: true)
          |> Repo.one
        end

      conn
      |> put_view(Lynx.LinkView)
      |> render(:index, links: links,
                        sort: sort,
                        unread_count: count,
                        tag: tag,
                        mini_title: tag.name,
                        title: tag.name <> ", #{sort}")
    end
  end

  def new(conn, _params) do
    changeset = Tag.changeset(%Tag{}, %{"ip_address" => remote_ip(conn)}, force: true)
    render(conn, title: "New Tag", changeset: changeset)
  end

  def create(conn, %{"tag" => tag_params}) do #, "g-recaptcha-response" => recaptcha
    changeset =
      Tag.changeset(%Tag{}, tag_params, force: true) #captcha: recaptcha, ip_address: remote_ip(conn)
      |> Changeset.change(ip_address: remote_ip(conn), user_id: current_user(conn).id)

    case Repo.insert(changeset) do
      {:ok, tag} ->
        conn
        |> put_flash(:success, "Tag created successfully.")
        |> redirect_json(tag_path(conn, :show, tag.name_indexed))
      {:error, changeset} ->
        json conn, %{success: false, errors: changeset_errors(changeset)}
    end
  end

  def edit(conn, %{"id" => id}) do
    tag = Repo.get_by!(Tag, name_indexed: id)
    render(conn, title: "edit tag", mini_title: "edit", changeset: Tag.changeset(tag), tag: tag)
  end

  def update(conn, %{"id" => id, "tag" => tag_params}) do
    tag = Repo.get_by!(Tag, name_indexed: id)
    changeset =
      Tag.changeset(tag, tag_params)
      |> Changeset.change(default: tag_params["default"] == "true")

    case Repo.update(changeset) do
      {:ok, tag} ->
        conn
        |> put_flash(:success, "Tag successfully updated.")
        |> redirect_json(tag_path(conn, :show, tag.name_indexed))
      {:error, changeset} ->
        json(conn, %{success: false, errors: changeset_errors(changeset)})
    end
  end

  def delete(conn, %{"id" => id}) do
    tag = Repo.get_by!(Tag, name_indexed: id)
    if tag.subscribers >= 25, do: raise "Could not delete, too many subscribers"
    Repo.delete(tag)
    conn
    |> put_flash(:success, "Tag deleted.")
    |> redirect(to: "/")
  end

  def search(conn, %{"search" => search}) do
    search =
      search
      |> String.downcase
      |> String.replace(~r/[^a-z0-9\/]+/, "")

    body =
      if search && search != "" do
        from t in Tag, order_by: [desc: t.subscribers, desc: t.name_indexed],
          where: like(t.name, ^"%#{search}%") or like(t.name_indexed, ^"%#{search}%")
      else
        from t in Tag, order_by: [desc: t.subscribers, desc: t.name_indexed]
      end
      |> Tag.join_subscriptions(current_user(conn).id)
      |> Repo.all
      |> Enum.map(fn(tag) ->
           Phoenix.View.render_to_string(Lynx.TagView, "_tag.html", conn: conn, tag: tag)
         end)
      |> Enum.join

      json(conn, %{success: true, html: body, search: search})
  end

  def autocomplete(conn, %{"search" => search, "exclude" => exclude}) do
    search =
      search
      |> String.downcase
      |> String.replace(~r/[^a-z0-9\/]+/, "")

    query = from t in Tag,
            left_join: s in UserSubscription, on: s.tag_id == t.id
              and s.user_id == ^current_user(conn).id,
            where: (like(t.name, ^"%#{search}%") or like(t.name_indexed, ^"%#{search}%"))
              and t.can_submit and not t.name_indexed in ^String.split(exclude, ","),
            limit: 6,
            select: t.name_indexed,
            order_by: [desc: s.user_id, desc: t.subscribers, desc: t.name_indexed]
    json(conn, %{success: true, tags: Repo.all(query), search: search})
  end
end