defmodule Lynx.UserSubscriptionController do
  use Lynx.Web, :controller
  plug :require_signed_in when action in [:create, :delete, :index]
  plug :scrub_params, "search" when action in [:search]
  plug :require_user when action in [:create, :delete, :index]
  plug :put_view, Lynx.UserView

  def index(conn, _params) do
    subscriptions =
      from(tag in Tag,
           left_join: us in UserSubscription, on: us.tag_id == tag.id and us.user_id == ^conn.assigns[:user].id,
           preload: [subscription: us],
           where: us.user_id > 0,
           order_by: tag.name_indexed)
      |> Repo.all
      |> Enum.group_by(fn(x) -> if x.subscription.blocked, do: :blocked, else: :subscribed end)

    render(conn, "subscriptions.html",
                 title: "Subscriptions",
                 subscriptions: subscriptions,
                 user: conn.assigns[:user])
  end

  def create(conn, %{"id" => name, "blocked" => blocked} = _params) do
    id = Repo.one!(from tag in Tag, where: tag.name_indexed == ^name, select: tag.id)

    %UserSubscription{}
    |> UserSubscription.changeset(%{tag_id: id, user_id: conn.assigns[:user].id, blocked: blocked})
    |> UserSubscription.insert!

    json(conn, %{success: true, sidebar: get_sidebar_html(conn)})
  end

  def delete(conn, %{"id" => name} = _params) do
    id = Repo.one!(from tag in Tag, where: tag.name_indexed == ^name, select: tag.id)

    UserSubscription
    |> Repo.get_by!(tag_id: id, user_id: conn.assigns[:user].id)
    |> UserSubscription.delete!

    json(conn, %{success: true, sidebar: get_sidebar_html(conn)})
  end

  defp get_sidebar_html(conn) do
    tags = Tag.subscribed_tags(current_user(conn).id)
    render_to_string(Lynx.LayoutView, "_sidebar.html", conn: conn, tags: tags)
  end

  defp require_user(conn, _params) do
    user = Repo.get_by!(User, name_lower: String.downcase(conn.params["user"] || ""))
    unless User.can_edit?(user, current_user(conn)), do: raise Lynx.NoPermission
    assign(conn, :user, user)
  end
end
