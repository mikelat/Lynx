defmodule Lynx.VoteController do
  use Lynx.Web, :controller
  alias Ecto.Changeset

  def create(conn, params) do
    if signed_in?(conn) do
      content = String.to_atom(params["content"] || "invalid")
      id =
        if content == :link do
          Link.slug_id_to_id(params["id"])
        else
          String.to_integer(params["id"] || "0")
        end

      unless content in Map.keys(Vote.valid_content_types) do
        raise "Invalid content type passed"
      end

      vote =
        from(v in Vote, where: ^["#{content}_id": id],
                        where: v.user_id == ^current_user(conn).id)
        |> Repo.one || %Vote{}

      changes = %{change: params["change"], referer: conn.req_headers[:referer]}

      # New record, cast content id
      if is_nil(vote.inserted_at) do
        Vote.changeset_defaults(vote, conn)
      else
        Changeset.change(vote)
      end
      |> Changeset.change(Map.put(changes, String.to_atom("#{content}_id"), id))
      |> Vote.change!

      json(conn, %{success: true})
    else
      json(conn, %{success: false, sign_in: true})
    end
  end
end
