defmodule Lynx.UserController do
  use Lynx.Web, :controller
  plug :require_signed_in when action in [:edit, :update]
  plug :scrub_params, "user" when action in [:create, :update]
  plug :set_abuse_score when action in [:new]
  plug :require_signed_out when action in [:new, :create]
  plug :require_user when action in [:edit, :update]

  def new(conn, _params) do
    render(conn, changeset: User.changeset(%User{}),
                 mini_title: "Sign in",
                 title: "Sign in/Register")
  end

  def create(conn, %{"user" => user_params, "g-recaptcha-response" => recaptcha}) do
    user_params = Map.put(user_params, "ip_address", remote_ip(conn))
    changeset =
      User.changeset(%User{}, user_params, captcha: recaptcha, ip_address: remote_ip(conn))
      |> Changeset.change(ip_address: remote_ip(conn))
    case User.insert(changeset) do
      {:ok, user} ->
        conn
        |> sign_in_user(user)
        |> put_flash(:success, "User created successfully and logged in.")
        |> redirect_json("/")
      {:error, changeset} ->
        json conn, %{success: false, errors: changeset_errors(changeset)}
    end
  end

  def show(conn, %{"id" => id}) do
    user = Repo.get_by!(User, name_lower: String.downcase(id || ""))
    render(conn, user: user,
                 title: "Profile of #{user.name}",
                 mini_title: user.name)
  end

  def edit(conn, _params) do
    render(conn, user: conn.assigns[:user],
                 changeset: User.changeset(conn.assigns[:user]),
                 title: "Settings")
  end

  def update(conn, %{"user" => user_params} = _params) do
    changeset = User.changeset(conn.assigns[:user], user_params)
    changeset =
      if user_params["admin"] && current_user(conn).admin && !conn.assigns[:user].admin do
        Changeset.change(changeset, banned: user_params["banned"] == "true")
      else
        changeset
      end
    case Repo.update(changeset) do
      {:ok, user} ->
        if user_params["update_password"] || user_params["update_profile"] do
          conn = cond do
            user_params["update_password"] -> put_flash(conn, :success, "Password successfully updated.")
            :else -> put_flash(conn, :success, "Profile successfully updated.")
          end
          redirect_json(conn, user_path(conn, :show, user.name_lower))
        else
          json(conn, %{success: true})
        end
      {:error, changeset} ->
        json conn, %{success: false, errors: changeset_errors(changeset)}
    end
  end

  defp require_user(conn, _params) do
    user = Repo.get_by!(User, name_lower: String.downcase(conn.params["id"] || ""))
    unless User.can_edit?(user, current_user(conn)), do: raise Lynx.NoPermission
    assign(conn, :user, user)
  end
end
