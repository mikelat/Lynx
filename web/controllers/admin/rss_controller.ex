defmodule Lynx.AdminRssController do
  use Lynx.Web, :controller

  def index(conn, _params) do
    bot = Repo.get(User, config(:bot_id))

    render(conn, bot: bot,
                 rss_feeds: Repo.all(from t in Rss),
                 title: "RSS feeds",
                 mini_title: "RSS")
  end

  def new(conn, _params) do
    render(conn, title: "New RSS feed",
                 mini_title: "RSS",
                 js_exec: "link",
                 changeset: Rss.changeset(%Rss{}))
  end

  def create(conn, %{"rss" => rss_params}) do
    changeset = Rss.changeset(%Rss{}, rss_params)

    case Repo.insert(changeset) do
      {:ok, rss} ->
        conn
        |> put_flash(:success, "rss feed created successfully.")
        |> redirect_json(admin_rss_path(conn, :show, rss.id))
      {:error, changeset} ->
        json conn, %{success: false, errors: changeset_errors(changeset)}
    end
  end

  def edit(conn, %{"id" => id}) do
    rss = Repo.get!(Rss, id)
    render(conn, title: "edit rss",
                 mini_title: "edit",
                 js_exec: "link",
                 changeset: Rss.changeset(rss),
                 rss: rss)
  end

  def update(conn, %{"id" => id, "rss" => rss_params}) do
    rss = Repo.get!(Rss, id)
    changeset = Rss.changeset(rss, rss_params)

    case Repo.update(changeset) do
      {:ok, rss} ->
        conn
        |> put_flash(:success, "Rss successfully updated.")
        |> redirect_json(admin_rss_path(conn, :show, rss.id))
      {:error, changeset} ->
        json(conn, %{success: false, errors: changeset_errors(changeset)})
    end
  end

  def show(conn, %{"id" => id}) do
    rss = Repo.get!(Rss, id)
    {:ok, feed} = Rss.fetch_feed(rss)
    feeds =
      Enum.map(feed, &(Map.put(&1, :title, Rss.parse_title(rss, &1))))
      |> Enum.filter(&(&1.title != ""))
      |> Enum.take(10)
    render(conn, rss: rss,
                 title: rss.name,
                 feeds: feeds,
                 mini_title: rss.name)
  end

  def delete(conn, %{"id" => id}) do
    Repo.get!(Rss, id) |> Repo.delete!()
    conn |> redirect(to: admin_rss_path(conn, :index))
  end
end