defmodule Lynx.AdminController do
  use Lynx.Web, :controller

  def index(conn, _params) do
    render(conn, title: "admin")
  end

end