defmodule Lynx.LinkReadController do
  use Lynx.Web, :controller

  def create(conn, params) do
    # TODO: Rate limit this to prevent abuse
    if signed_in?(conn) do
      now = Timex.now
      link_ids = Enum.map(params["ids"], &Link.slug_id_to_id/1)

      existing_ids =
        from(lr in LinkRead, select: lr.link_id,
                             where: lr.link_id in ^link_ids and lr.user_id == ^current_user(conn).id)
        |> Repo.all

      link_inserts =
        Enum.map(link_ids -- existing_ids, fn(id) ->
          [link_id: id, user_id: current_user(conn).id, inserted_at: now]
        end)
      Repo.insert_all(LinkRead, link_inserts)

      count =
        from(l in Link, select: count(l.id),
                        where: l.id in ^(link_ids -- existing_ids) and l.score >= 0)
        |> Repo.one

      User.change_unread_count(current_user(conn).id, count * -1)
      json(conn, %{success: true})
    else
      json(conn, %{success: false, sign_in: true})
    end
  end

  def delete(conn, params) do
    {count, _} =
      from(rv in LinkRead, where: rv.link_id == ^Link.slug_id_to_id(params["id"])
                             and rv.user_id == ^current_user(conn).id)
      |> Repo.delete_all

    User.change_unread_count(current_user(conn).id, count)
    json(conn, %{success: true})
  end
end