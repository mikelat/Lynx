defmodule Lynx.SessionController do
  use Lynx.Web, :controller
  plug :put_view, Lynx.UserView
  plug :scrub_params, "sign_in" when action in [:create]
  plug :require_signed_in when action in [:delete]
  plug :set_abuse_score when action in [:create]
  plug :require_signed_out when action in [:create]

  # Sign in
  def create(conn, %{"sign_in" => user_params} = params) do
    user = Repo.get_by(User, name_lower: String.downcase(user_params["name"] || ""))
    {c_status, c_msg} = abuse_check_captcha(conn, risky: true, response: params["g-recaptcha-response"])

    if c_status && User.authenticate(user, user_params["password"]) do
      conn
      |> put_flash(:success, "Welcome back, #{user.name}!")
      |> sign_in_user(user)
      |> redirect_json("/")
    else
      {score, errors} =
        case c_status do
          true  -> {abuse_increment!(conn), %{base: ["Username or password is invalid"]}}
          false -> {conn.private[:abuse_score], %{base: [c_msg]}}
        end
      json(conn, %{success: false, errors: errors, abuse_score: score})
    end
  end

  # Sign out
  def delete(conn, _) do
    conn
    |> put_flash(:success, "Goodbye, #{current_user(conn).name}!")
    |> sign_out_user
    |> redirect_json("/")
  end

  # Quirk: So regular phoenix 404s do not call plugs (which load our user info and things like that)
  # so since we need that stuff we redirect all undefined routes here and call it manually
  def not_found(conn, _) do
    raise Phoenix.Router.NoRouteError, conn: conn, router: __MODULE__
  end
end
