defmodule Lynx.NoPermission do
  defexception message: "You do not have permission to access this page"
end

defmodule Lynx.AdminRequired do
  defexception message: "You do not have permission to access this page"
end

defimpl Plug.Exception, for: Lynx.AdminRequired do
  def status(_exception), do: 404
end