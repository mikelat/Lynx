defmodule Lynx do
  use Application

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec

    # Change base URL in production to force HTTPS on any link
    if Application.fetch_env!(:lynx, :mix_env) == :prod do
      if System.get_env("URL") do
        new_endpoint_config =
          Application.fetch_env!(:lynx, Lynx.Endpoint)
          |> Keyword.put(:url, [host: System.get_env("URL"), scheme: "https"])
        Application.put_env(:lynx, Lynx.Endpoint, new_endpoint_config, persistent: true)
      end

      Application.put_env(:sentry, :environment_name, String.to_atom(System.get_env("SERVER_ENV")), persistent: true)
      Application.put_env(:sentry, :tags, %{ env: System.get_env("SERVER_ENV") }, persistent: true)
    end

    # Define workers and child supervisors to be supervised
    children = [
      # Start the Ecto repository
      supervisor(Lynx.Repo, []),
      # Start the endpoint when the application starts
      supervisor(Lynx.Endpoint, []),
      # Start your own worker by calling: Lynx.Worker.start_link(arg1, arg2, arg3)
      # worker(Lynx.Worker, [arg1, arg2, arg3]),
      worker(Cachex, [:lynx, []]),
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Lynx.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    Lynx.Endpoint.config_change(changed, removed)
    :ok
  end
end
