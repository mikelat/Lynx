defmodule Lynx.Jobs.Hot do
  alias Lynx.{Repo, Link, Vote}
  import Ecto.Query, only: [from: 2]
  require Logger

  def run(opts \\ []) do
    Sentry.Context.set_tags_context(%{job: "hot"})
    opts = Keyword.merge([all: false, verbose: false], opts)
    query = from l in Link, where: l.inserted_at > datetime_add(^Ecto.DateTime.utc, -5, "day")
    links =
      if opts[:all] do
        Repo.all(query, log: opts[:verbose])
      else
        from(l in query, where: l.updated_at < datetime_add(^Ecto.DateTime.utc, -1, "hour"))
        |> Repo.all(log: opts[:verbose])
      end

    if opts[:verbose], do: Logger.info("Recalculating on #{Enum.count(links)} link(s)")

    Enum.each(links, fn(link) ->
      link
      |> Ecto.Changeset.change(hot: Vote.calculate_hot(link))
      |> Repo.update!(log: opts[:verbose])
    end)
    :ok
  end
end