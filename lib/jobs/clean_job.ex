defmodule Lynx.Jobs.Clean do
  alias Lynx.{Repo, LinkRead}
  import Ecto.Query, only: [from: 2]
  require Logger

  def run(opts \\ []) do
    Sentry.Context.set_tags_context(%{job: "clean"})
    opts = Keyword.merge([verbose: false], opts)

    if opts[:verbose], do: Logger.info("Deleting old read records...")

    from(lr in LinkRead, where: lr.inserted_at < datetime_add(^Ecto.DateTime.utc, -11, "day") and lr.id != 1)
    |> Repo.delete_all
    :ok
  end
end
