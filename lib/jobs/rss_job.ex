defmodule Lynx.Jobs.Rss do
  alias Lynx.{Repo, Link, Rss}
  alias Ecto.Changeset
  import Lynx.Utils
  import Ecto.Query, only: [from: 2]
  require Logger

  def run(opts \\ []) do
    Repo.start_link
    opts = Keyword.merge([verbose: false, from_task: false], opts)
    {:ok, _started} = Application.ensure_all_started(:timex)

    from(r in Rss, where: (r.next_parse <= ^Timex.now or is_nil(r.next_parse)) and r.active == true)
    |> Repo.all(log: opts[:verbose])
    |> Enum.each(fn(rss) ->
      Sentry.Context.add_breadcrumb(%{feed: rss.url})
      Sentry.Context.set_tags_context(%{job: "rss"})
      {resp, feed} = Rss.fetch_feed(rss)
      if resp == :ok do
        hashes = String.split(rss.hashes, ",")

        # Filter items so only new items are in the list
        items = Enum.reject(feed, fn(item) -> Enum.member?(hashes, Rss.hash(item)) end) |> Enum.reverse

        # Remove any hashes not in the feed, leaving behind orphans, which we remove from our stored hashes
        hashes = hashes -- (hashes -- Enum.map(feed, &(Rss.hash/1)))

        if items == [] do
          Changeset.change(rss, next_parse: next_parse(rss))
        else
          parse_item(rss, hd(items))
          Changeset.change(rss, last_parse: Timex.now,
                                hashes: Enum.join(hashes ++ [Rss.hash(hd(items))], ","),
                                next_parse: next_parse(rss))
        end
        |> Repo.update!(log: opts[:verbose])
      end
    end)

    if opts[:from_task] do
      if opts[:verbose], do: Logger.info("Waiting for thumbnails to generate...")
      wait_for_processes()
    end
    :ok
  end

  defp follow_url(url) do
    {url, 0} = System.cmd("timeout", ["20", "curl", url, "-s", "-L", "-I", "-o", "/dev/null", "-w", "%{url_effective}"])
    url
  end

  defp parse_item(rss, item) do
    # Only add link if nobody has submitted the link ever
    query = from(l in Link, where: l.url == ^item.link, select: count(l.id))
    if Repo.one(query) == 0 && Rss.parse_title(rss, item) != "" do
      Link.changeset(%Link{}, %{title: Rss.parse_title(rss, item),
                                url: if(rss.clean_url, do: follow_url(item.link), else: item.link),
                                tag_list: rss.tag_list},
                     force: true)
      |> Changeset.change(ip_address: "0.0.0.0", user_id: config(:bot_id))
      |> Link.insert!(log: false)
    end
  end

  defp next_parse(rss) do
    Timex.shift(Timex.now, minutes: Enum.random(rss.minutes_lower..rss.minutes_upper))
  end

  defp wait_for_processes do
    count = Enum.count Process.registered(), fn(x) -> String.starts_with?(to_string(x), "lynx_link_") end
    if count > 0 do
      :timer.sleep(250)
      wait_for_processes()
    end
  end
end