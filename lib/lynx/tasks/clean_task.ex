defmodule Mix.Tasks.Lynx.Clean do
  use Mix.Task
  require Logger
  alias Lynx.Repo

  def run(params \\ [])
  def run(params) when is_atom(params), do: run([to_string(params)])
  def run(params) when is_binary(params), do: run(String.split(params, " "))
  def run(params) do
    silent = "silent" in params
    Repo.start_link
    unless silent, do: Logger.info("Clean up started...")

    Lynx.Jobs.Clean.run(verbose: !silent, from_task: true)

    unless silent, do: Logger.info("Clean up completed!")
  end
end