defmodule Mix.Tasks.Lynx.Rss do
  use Mix.Task
  require Logger
  alias Lynx.Repo

  def run(params \\ [])
  def run(params) when is_atom(params), do: run([to_string(params)])
  def run(params) when is_binary(params), do: run(String.split(params, " "))
  def run(params) do
    silent = "silent" in params
    Repo.start_link

    unless silent, do: Logger.info "Starting RSS checks..."
    Lynx.Jobs.Rss.run(verbose: !silent, from_task: true)
    unless silent, do: Logger.info "RSS checks completed!"
  end
end