defmodule Mix.Tasks.Lynx.Hot do
  use Mix.Task
  require Logger
  alias Lynx.Repo

  def run(params \\ [])
  def run(params) when is_atom(params), do: run([to_string(params)])
  def run(params) when is_binary(params), do: run(String.split(params, " "))
  def run(params) do
    silent = "silent" in params
    Repo.start_link
    unless silent do
      if "all" in params do
        Logger.info("Starting hotness on ALL links...")
      else
        Logger.info("Starting hotness on links...")
      end
    end

    Lynx.Jobs.Hot.run(verbose: !silent, all: "all" in params)

    unless silent, do: Logger.info("Recalculated hotness on links")
  end
end