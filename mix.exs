defmodule Lynx.Mixfile do
  use Mix.Project

  def project do
    [app: :lynx,
     version: "1.0.0",
     elixir: "~> 1.4",
     elixirc_paths: elixirc_paths(Mix.env),
     compilers: [:phoenix, :gettext] ++ Mix.compilers,
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     aliases: aliases(),
     deps: deps()]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [mod: {Lynx, []},
     applications: [:phoenix, :phoenix_pubsub, :phoenix_html, :phoenix_ecto, :sentry, :cachex,
                    :mariaex, :timex, :cowboy, :logger, :gettext, :logger_file_backend,
                    :httpoison, :feeder_ex, :xmerl, :quantum, :edeliver],
     included_applications: [:comeonin, :hashids, :elixilorem, :phoenix_slime, :timex_ecto, :floki]]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "web", "test/support"]
  defp elixirc_paths(_),     do: ["lib", "web"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do

    [{:phoenix, "~> 1.2.1"},
     {:phoenix_pubsub, "~> 1.0"},
     {:phoenix_ecto, "~> 3.0"},
     {:phoenix_html, "~> 2.6"},
     {:phoenix_live_reload, "~> 1.0", only: :dev},
     {:gettext, "~> 0.11"},
     {:cowboy, "~> 1.0"},
     {:mariaex, ">= 0.0.0"},
     # Lynx Deps
     {:phoenix_slime, ">= 0.6.0"},
     {:cachex, "~> 2.0"},
     {:comeonin, ">= 1.3.0"},
     {:logger_file_backend, ">= 0.0.0"},
     {:elixilorem, ">= 0.0.0"},
     {:quantum, ">= 1.9.0"},
     {:timex, "~> 3.0"},
     {:timex_ecto, "~> 3.0"},
     {:hashids, ">= 2.0.0"},
     {:feeder_ex, ">= 0.0.0"},
     {:httpoison, ">= 0.7.2"},
     {:floki, ">= 0.0.0"},
     {:feeder, "< 2.2.0", override: true}, # remove this when feeder is fixed
     # Builds
     {:sentry, "~> 2.0"},
     {:distillery, ">= 0.0.0"},
     {:edeliver, "~> 1.4.1"}]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    ["ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
     "ecto.reset": ["ecto.drop", "ecto.setup"],
     "test":       ["ecto.create --quiet", "ecto.migrate", "test"],
     "lynx.reset": ["ecto.drop", "ecto.setup", "run priv/repo/seeds.exs"],
     "s":          ["phoenix.server"]]
  end
end
