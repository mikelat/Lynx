# Lynx

To start your Lynx:
  * apt-get install -y -q wget curl git make build-essential cutycapt pngquant subversion \
    mysql-client apt-listchanges xvfb libqt4-webkit libqt4-dev nano xfonts-scalable xfonts-100dpi \
    libgl1-mesa-dri g++ imagemagick
  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Install Node.js dependencies with `npm install`
  * Start Phoenix endpoint with `mix phoenix.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](http://www.phoenixframework.org/docs/deployment).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
