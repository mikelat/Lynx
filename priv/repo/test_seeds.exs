alias Lynx.Repo
alias Lynx.User
alias Lynx.Category

params = %{name: "admin", ip_address: "0.0.0.0", password: "password", password_confirmation: "password"}
force_params = %{admin: true, weight_up: 10.0, weight_down: 5.0}
Repo.insert!(Ecto.Changeset.change(User.changeset(%User{}, params), force_params))

params = %{name: "user", ip_address: "0.0.0.0", password: "password", password_confirmation: "password"}
Repo.insert!(User.changeset(%User{}, params))

Repo.insert! Category.changeset(%Category{}, %{name: "Main"})
Repo.insert! Category.changeset(%Category{}, %{name: "Sub", category_id: 1})