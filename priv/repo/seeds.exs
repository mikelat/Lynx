# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Lynx.Repo.insert!(%SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

defmodule Seed do
  alias Lynx.{Repo, Tag, Comment, Link, User, UserSubscription}
  require Logger
  import Lynx.Utils
  use Timex

  def run do
    #unless Application.fetch_env!(:lynx, :env) == :dev || Application.fetch_env!(:lynx, :env) == :test, do: raise "NOT IN DEV! THIS TASK IS VERY DANGEROUS. ABORTING."
    Logger.configure([level: :info])
    File.rm_rf(config(:thumbnails_path))
    Repo.start_link

    Logger.info "Creating tags..."

    tags =
      Enum.map(["games", "pc", "xbox", "playstation", "nintendo", "dota 2", "technology",
        "gadgets", "software", "hardware", "programming", "science", "astronomy", "biology",
        "physics", "business", "entrepreneurs", "finance", "startup"], fn(name) ->
        Tag.changeset(%Tag{}, %{name: name})
        |> Ecto.Changeset.change(default: true)
        |> Repo.insert!
      end)

    Logger.info "Creating users..."

    Enum.each(Enum.map(tags, fn(x) -> Map.get(x, :id) end), fn(x) ->
      Repo.insert(%UserSubscription{user_id: 1, tag_id: x})
      Repo.insert(%UserSubscription{user_id: 2, tag_id: x})
    end)
    Enum.map(1..4, fn(x) -> create_user(%{name: "user" <> Integer.to_string(x)}) end)

    Logger.info "Creating links..."
    tags = Enum.map(tags, fn(x) -> Map.get(x, :name_indexed) end)
    create_link(%{title: "Netflix launched a new layout!", url: "https://movies.netflix.com/", tag_list: make_tags(tags)})
    create_link(%{title: "Some tech article and a really long name in the title to test the trim of slugs thx", url: "http://techcrunch.com/2015/08/18/when-the-bus-will-be-its-own-boss/", tag_list: make_tags(tags)})
    create_link(%{title: "I put code here sometimes", url: "https://www.gitlab.com/", tag_list: make_tags(tags)})
    create_link(%{title: "Oh hey look its a link to google", url: "http://www.google.com", tag_list: make_tags(tags)})
    create_link(%{title: "Path of exile got a 2.0 release!", url: "http://www.pathofexile.com", tag_list: make_tags(tags)})
    create_link(%{title: "A picture of a crazy guy", url: "http://i.imgur.com/NH0kxQ6.jpg", tag_list: make_tags(tags)})
    create_link(%{title: "mario party 2", url: "https://www.youtube.com/watch?v=m6PxRwgjzZw&", tag_list: make_tags(tags)})
    create_link(%{title: "hello", url: "https://www.youtube.com/watch?v=NL6CDFn2i3I", tag_list: make_tags(tags)})
    create_link(%{title: "this is a random youtube link", url: "https://www.youtube.com/watch?v=E8sxwK2pJI4", tag_list: make_tags(tags)})
    create_link(%{title: "a song to test embed", url: "https://soundcloud.com/neal-acree/starcraft-ii-heart-of-the", tag_list: make_tags(tags)})
    create_link(%{title: "textual post", text: "this is a text post", tag_list: make_tags(tags)})
    create_link(%{title: "!!!!!!!!", text: "this is a text post", tag_list: make_tags(tags)})
    create_link(%{title: "devdocs!", url: "https://www.devdocs.io", tag_list: make_tags(tags)})
    create_link(%{title: "some hosting site!", url: "https://www.linode.com", tag_list: make_tags(tags)})
    Enum.map(1..100, fn(_) -> create_spam_link(%{tag_list: make_tags(tags)}) end)

    Logger.info "Creating comments..."
    Enum.map(1..75, fn(_) -> create_comment() end)
    Enum.map(1..75, fn(_) -> create_comment(%{comment_id: Enum.random(1..75)}) end)
    Enum.map(1..50, fn(_) -> create_comment(%{comment_id: Enum.random(75..150)}) end)
    Enum.map(1..50, fn(_) -> create_comment(%{comment_id: Enum.random(150..200)}) end)

    Logger.info "Calculating hotness on links..."
    Lynx.Jobs.Hot.run(all: true)
    Logger.info "Waiting for thumbnails..."
    wait_for_processes()
    Logger.info "All done! Enjoy your seeded application."
    :ok
  end

  def make_tags(tags) do
    tags |> Enum.take_random(Enum.random(1..5)) |> Enum.join(",")
  end

  def wait_for_processes do
    count = Enum.count Process.registered(), fn(x) -> String.starts_with?(to_string(x), "lynx_link_") end
    if count > 0 do
      :timer.sleep(250)
      wait_for_processes()
    end
  end

  def create_comment(params \\ %{}, force_params \\ %{}) do
    score_up = Enum.random(0..60)
    score_down = Enum.random(0..30)
    params = Map.merge(%{text: Elixilorem.paragraph,
                         link_id: Enum.random(1..15)}, params)
    force_params = Map.merge(%{votes_count: Enum.random(5..15),
                               score_up: score_up + 0.0,
                               score_down: score_down + 0.0,
                               score: score_up - score_down,
                               user_id: Enum.random(1..6),
                               ip_address: "0.0.0.0",
                               inserted_at: random_datetime()}, force_params)
    changeset = Comment.changeset(%Comment{}, params, force: true)
    Comment.insert!(Ecto.Changeset.change(changeset, force_params))
  end

  def create_user(params \\ %{}, force_params \\ %{}) do
    params = Map.merge(%{ip_address: "0.0.0.0", password: "password", password_confirmation: "password"}, params)
    User.changeset(%User{}, params, force: true)
    |> Ecto.Changeset.change(force_params)
    |> User.insert!
  end

  def create_link(params \\ %{}, force_params \\ %{}) do
    score_up = Enum.random(0..300)
    score_down = Enum.random(0..100)
    force_params = Map.merge(%{user_id: Enum.random(1..6),
                               votes_count: Enum.random(20..30),
                               score_up: score_up + 0.0,
                               score_down: score_down + 0.0,
                               score: score_up - score_down,
                               ip_address: "0.0.0.0",
                               nsfw: Enum.random(1..4) == 1,
                               inserted_at: random_datetime()}, force_params)
    changeset = Link.changeset(%Link{}, params, force: true)
    changeset
    |> Ecto.Changeset.change(force_params)
    |> Link.insert!
  end

  def create_spam_link(params \\ %{}, force_params \\ %{}) do
    score_up = Enum.random(0..30)
    score_down = Enum.random(0..20)
    params = Map.merge(%{title: Elixilorem.words(4),
                         text: Elixilorem.sentence}, params)
    force_params = Map.merge(%{user_id: Enum.random(1..6),
                               ip_address: "0.0.0.0",
                               votes_count: Enum.random(10..20),
                               score_up: score_up + 0.0,
                               score_down: score_down + 0.0,
                               score: score_up - score_down,
                               nsfw: Enum.random(1..4) == 1,
                               inserted_at: random_datetime()}, force_params)
    changeset = Link.changeset(%Link{}, params, force: true)
    changeset
    |> Ecto.Changeset.change(force_params)
    |> Link.insert!
  end

  def random_datetime do
    Timex.now |> Timex.shift(minutes: Enum.random(0..-4320)) # 3 days
  end
end

Seed.run