defmodule Lynx.Repo.Migrations.Init do
  use Ecto.Migration

  def change do
    # Users
    create table(:users) do
      add :name, :string
      add :name_lower, :string
      add :password_encrypted, :string
      add :email, :string
      add :ip_address, :string
      add :profile, :text
      add :profile_cached, :text
      add :admin, :boolean, default: false
      add :show_nsfw, :boolean, default: false
      add :unread_enable, :boolean, default: true
      add :unread_scroll, :boolean, default: true
      add :unread_click, :boolean, default: true
      add :unread_comment, :boolean, default: true
      add :comment_score, :float, default: 0.0
      add :link_score, :float, default: 0.0
      add :comment_count, :integer, default: 0
      add :link_count, :integer, default: 0
      add :weight_up, :float, default: 1.0
      add :weight_down, :float, default: 1.0
      add :unread_count, :integer, default: 0
      add :unread_date, :datetime
      add :banned, :boolean, default: false

      timestamps null: true
    end
    create unique_index(:users, [:name_lower])

    # Tags
    create table(:tags) do
      add :name, :string
      add :name_indexed, :string
      add :subscribers, :integer, default: 0
      add :default, :boolean, default: false
      add :can_submit, :boolean, default: true
      add :ip_address, :string

      add :user_id, :integer

      timestamps null: true, null: true
    end
    create unique_index(:tags, [:name_indexed])
    create index(:tags, [:name])
    create index(:tags, [:default])

    # Links
    create table(:links) do
      add :title, :string
      add :slug, :string
      add :slug_id, :string
      add :url, :string
      add :domain, :string
      add :ip_address, :string
      add :text, :text
      add :text_cached, :text
      add :thumbnail, :string
      add :thumbnail_failed, :boolean, default: false
      add :thumbnail_generating, :boolean, default: false
      add :spoiler, :boolean, default: false
      add :comments_count, :integer, default: 0
      add :votes_count, :integer, default: 0
      add :content_type, :integer, default: 0
      add :score_up, :float, default: 0.0
      add :score_down, :float, default: 0.0
      add :score, :integer, default: 0
      add :nsfw, :boolean, default: false
      add :hot, :float, default: 0.0
      add :deleted, :int, default: 0
      add :deleted_date, :datetime

      add :user_id, references(:users)

      timestamps null: true
    end
    create index :links, [:hot]
    #create index :links, [:slug]
    create index :links, [:score]
    create index :links, [:domain]
    create index :links, [:url]
    create index :links, [:inserted_at]

    # Link Tags
    create table(:links_tags, primary_key: false) do
      add :tag_id, references(:tags)
      add :link_id, references(:links)
    end

    # User abuses
    create table(:users_abuses, primary_key: false) do
      add :ip_address, :string, primary_key: true
      add :score, :integer, default: 0

      timestamps null: true
    end
    create index(:users_abuses, [:ip_address, :inserted_at])

    # User auths
    create table(:users_auths) do
      add :token, :string
      add :user_id, references(:users)
    end
    create index(:users_auths, [:token])

    # Comments
    create table(:comments) do
      add :text, :text
      add :text_cached, :text
      add :ip_address, :string
      add :depth, :integer, default: 0
      add :deleted, :boolean, default: false
      add :votes_count, :integer, default: 0
      add :score_up, :float, default: 0.0
      add :score_down, :float, default: 0.0
      add :score, :integer, default: 0
      add :hot, :float, default: 0.0

      add :user_id, references(:users)
      add :link_id, references(:links)
      add :comment_id, references(:comments)

      timestamps null: true
    end

    # Votes
    create table(:votes) do
      add :weight_up, :float, default: 0.0
      add :weight_down, :float, default: 0.0
      add :change, :integer, default: 0
      #add :referer, :string
      add :ip_address, :string

      add :user_id, references(:users)
      add :link_id, references(:links)
      add :comment_id, references(:comments)

      timestamps null: true
    end
    create index :votes, [:inserted_at]
    create unique_index :votes, [:user_id, :link_id]
    create unique_index :votes, [:user_id, :comment_id]

    # Read
    create table(:links_read) do
      add :user_id, :integer
      add :link_id, :integer
      add :inserted_at, :datetime
    end
    create index :links_read, [:user_id]
    create index :links_read, [:link_id]
    create unique_index :links_read, [:user_id, :link_id]

    # User Subscriptions
    create table(:users_subscriptions) do
      add :blocked, :boolean, default: false

      add :user_id, references(:users)
      add :tag_id, references(:tags)

      timestamps null: true
    end
    create index(:users_subscriptions, [:user_id, :tag_id], unique: true)

    create table(:rss) do
      add :name, :string
      add :url, :string
      add :title, :string, default: "{title}"
      add :search, :string
      add :replace, :string
      add :match, :string
      add :tag_list, :string
      add :next_parse, :datetime
      add :last_parse, :datetime
      add :hashes, :text
      add :minutes_lower, :integer, default: 30
      add :minutes_upper, :integer, default: 45
      add :active, :boolean, default: false
      add :clean_url, :boolean, default: false

      timestamps null: true
    end

    create index :rss, [:next_parse]
  end
end
