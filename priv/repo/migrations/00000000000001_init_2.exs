defmodule Lynx.Repo.Migrations.Init2 do
  use Ecto.Migration
  alias Lynx.{Repo, LinkRead, User}
  import Ecto.Changeset, only: [change: 2]

  def up do
    Application.ensure_all_started(:tzdata)
    :ok = Application.ensure_started(:tzdata)

    %LinkRead{}
    |> change(link_id: 0, user_id: 0)
    |> Repo.insert!

    create_user(name: "lat",
                 password: "password",
                 password_confirmation: "password",
                 admin: true)

    create_user(name: "pat",
                 password: "password",
                 password_confirmation: "password",
                 admin: true)

    create_user(name: "bot",
                 password: "password",
                 password_confirmation: "password",
                 admin: true)
  end

  defp create_user(params) do
    %User{}
    |> User.changeset(Enum.into(params, %{}), force: true)
    |> change(admin: params[:admin], ip_address: "0.0.0.0")
    |> User.insert!
  end
end
