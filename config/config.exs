# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :lynx, Lynx.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "1V9suf5hr/3qSUI1qcsPFhKXZuu9tBq7H+mYnjWUij5TLYyw3vPMSXre/jrnYz7a",
  render_errors: [view: Lynx.ErrorView, accepts: ~w(html json)],
  root: ".",
  server: true,
  version: Mix.Project.config[:version],
  pubsub: [name: Lynx.PubSub,
           adapter: Phoenix.PubSub.PG2]

# General application configuration
config :lynx,
  check_captcha: {:system, "CHECK_CAPTCHA", true},
  site_name: {:system, "SITE_NAME", "LatFeed"},
  tmp_path: {:system, "TMP_PATH", "/tmp/"},
  thumbnails_path: {:system, "THUMBNAILS_PATH", "#{System.cwd!}/priv/static/images/thumbnails/"},
  thumbnails_url: {:system, "THUMBNAILS_URL", "/images/thumbnails/"},
  generate_thumbnails: true,
  ecto_repos: [Lynx.Repo],
  bot_id: 3, #user id of bot
  # The following keys are test and cause no captchas to spawn and always pass tests. This is for development/testing.
  # https://developers.google.com/recaptcha/docs/faq?hl=en#id-like-to-run-automated-tests-with-recaptcha-v2-what-should-i-do
  google_public_key: {:system, "GOOGLE_PUBLIC_KEY", "6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"},
  google_private_key: {:system, "GOOGLE_PRIVATE_KEY", "6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe"}

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id],
  truncated: :infinity


config :phoenix, :template_engines,
  slim: PhoenixSlime.Engine,
  slime: PhoenixSlime.Engine

config :quantum, :lynx, cron: [
    "*/15 * * * *": {Lynx.Jobs.Hot, :run},
    "* * * * *":    {Lynx.Jobs.Rss, :run},
    "@daily":       {Lynx.Jobs.Clean, :run}]

config :distillery,
  no_warn_missing: [:elixir_make]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
