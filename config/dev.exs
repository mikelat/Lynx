use Mix.Config

# For development, we disable any cache and enable
# debugging and code reloading.
#
# The watchers configuration can be used to run external
# watchers to your application. For example, we use it
# with brunch.io to recompile .js and .css sources.
config :lynx, Lynx.Endpoint,
  http: [port: 4000],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: [node: ["node_modules/brunch/bin/brunch", "watch", "--stdin",
                    cd: Path.expand("../", __DIR__)]],
  # Watch static and templates for browser reloading.
  live_reload: [
    patterns: [
      ~r{priv/static/[^thumbnails].*(js|css|png|jpeg|jpg|gif|svg)$},
      ~r{priv/gettext/.*(po)$},
      ~r{web/views/.*(ex)$},
      ~r{web/templates/.*(eex|slim)$}
    ]
  ]

config :lynx,
  mix_env: :dev,
  server_env: "dev",
  check_captcha: false,
  hashids_key: "dev"

# Do not include metadata nor timestamps in development logs
config :logger, :console,
  format: "$metadata[$level] $message\n",
  metadata: [:user_id, :ip_address]

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
config :phoenix, :stacktrace_depth, 20

# Configure your database
config :lynx, Lynx.Repo,
  adapter: Ecto.Adapters.MySQL,
  username: "root",
  password: "",
  database: "lynx_dev",
  hostname: "localhost",
  pool_size: 10
