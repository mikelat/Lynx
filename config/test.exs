use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :lynx, Lynx.Endpoint,
  http: [port: 4001],
  server: false

config :lynx,
  mix_env: :test,
  server_env: "test",
  check_captcha: false,
  hashids_key: "test"

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :lynx, Lynx.Repo,
  adapter: Ecto.Adapters.MySQL,
  username: "root",
  password: "",
  database: "lynx_test",
  hostname: "localhost",
  pool_size: 10
